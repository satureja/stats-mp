//---------------------------------------------------------------------------
/*            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.*/
#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "trayicon"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
void __fastcall TForm1::Memo1Click(TObject *Sender)
{   Memo1->Hide();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{  Memo1->Visible = ! Memo1->Visible;
    Memo1->BringToFront();
}
//---------------------------------------------------------------------------
// Remplir la ComboBox avec les mobs par d�faut
//---------------------------------------------------------------------------
void __fastcall TForm1::LoadDefautMobs(TObject *Sender)
{   TStringList *LB1 = new TStringList;
   try{  //use the string list
LB1->CommaText = "1002, Maggot, Maggot, 5, 50, 0, 0, 1, 1, 5, 10, 0, 5, 1, 1, 1, 0, 6, 5, 1, 1, 1, 3, 21, 129, 800, 1872, 672, 480, 505, 800, 501, 150, 518, 400, 533, 150, 502, 70, 522, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0";
sMonsters[0].sName = LB1->Strings[1];
sMonsters[0].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[0].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[0].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[0].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[0].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[0].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[0].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[0].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[0].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[0].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[0].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[0].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[0].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[0].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[0].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[0].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[0].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[0].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[0].sName);

LB1->CommaText = "1064, Bandit, Bandit, 20, 400, 0, 0, 21, 1, 40, 40, 5, 0, 10, 10, 10, 10, 10, 10, 1, 1, 1, 0, 29, 135, 500, 1500, 672, 900, 4016, 800, 521, 200, 526, 500, 535, 200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 30";
sMonsters[1].sName = LB1->Strings[1];
sMonsters[1].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[1].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[1].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[1].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[1].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[1].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[1].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[1].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[1].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[1].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[1].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[1].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[1].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[1].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[1].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[1].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[1].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[1].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[1].sName);

LB1->CommaText = "1018, Pinkie, Pinkie, 30, 300, 0, 0, 7, 2, 15, 20, 0, 5, 1, 1, 1, 0, 20, 25, 2, 2, 1, 3, 20, 129, 800, 1872, 672, 480, 614, 1000, 751, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 50";
sMonsters[2].sName = LB1->Strings[1];
sMonsters[2].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[2].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[2].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[2].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[2].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[2].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[2].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[2].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[2].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[2].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[2].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[2].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[2].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[2].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[2].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[2].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[2].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[2].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[2].sName);

LB1->CommaText = "1025, LogHead, LogHead, 30, 500, 0, 0, 5, 1, 10, 20, 50, 5, 1, 1, 1, 0, 20, 25, 1, 1, 1, 3, 22, 129, 800, 1872, 672, 480, 569, 2000, 740, 2500, 743, 3000, 569, 2000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 50";
sMonsters[3].sName = LB1->Strings[1];
sMonsters[3].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[3].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[3].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[3].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[3].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[3].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[3].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[3].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[3].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[3].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[3].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[3].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[3].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[3].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[3].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[3].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[3].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[3].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[3].sName);

LB1->CommaText = "1041, Snail, Snail, 30, 900, 0, 0, 18, 1, 50, 65, 20, 15, 10, 1, 25, 0, 20, 10, 1, 1, 1, 0, 20, 129, 1800, 2500, 672, 480, 719, 500, 806, 400, 1250, 1000, 1251, 1500, 1248, 500, 1252, 2000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 40";
sMonsters[4].sName = LB1->Strings[1];
sMonsters[4].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[4].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[4].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[4].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[4].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[4].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[4].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[4].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[4].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[4].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[4].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[4].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[4].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[4].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[4].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[4].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[4].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[4].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[4].sName);

LB1->CommaText = "1113, Pollett, Pollett, 30, 500, 0, 0, 7, 1, 6, 10, 1, 5, 1, 2, 1, 0, 20, 25, 1, 1, 1, 3, 20, 129, 800, 1600, 672, 480, 4026, 1000, 4023, 500, 700, 300, 862, 100, 1189, 400, 1189, 400, 1189, 400, 611, 500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 40";
sMonsters[5].sName = LB1->Strings[1];
sMonsters[5].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[5].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[5].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[5].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[5].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[5].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[5].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[5].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[5].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[5].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[5].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[5].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[5].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[5].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[5].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[5].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[5].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[5].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[5].sName);

LB1->CommaText = "1005, GreenSlime, GreenSlime, 40, 800, 0, 0, 96, 1, 80, 120, 20, 30, 20, 30, 10, 10, 30, 30, 1, 1, 1, 0, 21, 175, 1200, 1872, 672, 480, 502, 200, 4004, 20, 501, 100, 521, 250, 522, 200, 526, 500, 503, 500, 719, 750, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 20";
sMonsters[6].sName = LB1->Strings[1];
sMonsters[6].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[6].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[6].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[6].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[6].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[6].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[6].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[6].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[6].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[6].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[6].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[6].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[6].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[6].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[6].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[6].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[6].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[6].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[6].sName);

LB1->CommaText = "1007, YellowSlime, YellowSlime, 60, 400, 0, 0, 9, 1, 35, 40, 2, 7, 9, 6, 2, 1, 34, 1, 1, 1, 1, 0, 21, 131, 1400, 1800, 672, 480, 534, 200, 519, 100, 501, 350, 502, 250, 522, 10, 640, 450, 4006, 20, 4001, 450, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 20";
sMonsters[7].sName = LB1->Strings[1];
sMonsters[7].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[7].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[7].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[7].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[7].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[7].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[7].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[7].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[7].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[7].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[7].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[7].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[7].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[7].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[7].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[7].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[7].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[7].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[7].sName);

LB1->CommaText = "1008, RedSlime, RedSlime, 70, 450, 0, 0, 17, 1, 40, 45, 2, 7, 12, 8, 2, 1, 25, 1, 1, 1, 1, 0, 21, 135, 1300, 1500, 672, 480, 1201, 300, 509, 110, 521, 200, 4003, 20, 525, 80, 535, 750, 528, 250, 531, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 20";
sMonsters[8].sName = LB1->Strings[1];
sMonsters[8].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[8].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[8].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[8].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[8].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[8].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[8].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[8].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[8].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[8].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[8].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[8].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[8].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[8].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[8].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[8].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[8].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[8].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[8].sName);

LB1->CommaText = "1028, Mouboo, Mouboo, 85, 1200, 0, 0, 16, 2, 30, 75, 65, 5, 15, 5, 15, 0, 40, 10, 2, 2, 1, 3, 20, 129, 600, 1872, 672, 480, 660, 1000, 660, 500, 541, 750, 660, 50, 806, 500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 30";
sMonsters[9].sName = LB1->Strings[1];
sMonsters[9].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[9].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[9].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[9].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[9].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[9].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[9].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[9].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[9].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[9].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[9].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[9].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[9].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[9].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[9].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[9].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[9].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[9].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[9].sName);

LB1->CommaText = "1013, EvilMushroom, EvilMushroom, 110, 650, 0, 0, 35, 1, 65, 80, 4, 6, 16, 12, 10, 10, 35, 10, 1, 1, 1, 3, 22, 137, 800, 1800, 672, 480, 535, 500, 540, 10, 534, 100, 566, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 60";
sMonsters[10].sName = LB1->Strings[1];
sMonsters[10].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[10].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[10].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[10].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[10].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[10].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[10].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[10].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[10].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[10].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[10].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[10].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[10].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[10].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[10].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[10].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[10].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[10].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[10].sName);

LB1->CommaText = "1014, PinkFlower, PinkFlower, 115, 700, 0, 0, 40, 2, 70, 75, 0, 5, 16, 12, 20, 20, 20, 50, 2, 2, 1, 3, 22, 128, 800, 800, 672, 480, 535, 100, 540, 10, 1199, 100, 526, 400, 565, 1000, 565, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 50";
sMonsters[11].sName = LB1->Strings[1];
sMonsters[11].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[11].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[11].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[11].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[11].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[11].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[11].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[11].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[11].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[11].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[11].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[11].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[11].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[11].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[11].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[11].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[11].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[11].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[11].sName);


LB1->CommaText = "1009, BlackScorpion, BlackScorpion, 100, 600, 0, 0, 37, 1, 60, 100, 4, 6, 16, 30, 10, 10, 35, 10, 1, 1, 1, 0, 20, 133, 1000, 1500, 672, 480, 523, 150, 509, 100, 518, 800, 709, 800, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 50";
sMonsters[12].sName = LB1->Strings[1];
sMonsters[12].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[12].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[12].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[12].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[12].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[12].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[12].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[12].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[12].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[12].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[12].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[12].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[12].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[12].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[12].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[12].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[12].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[12].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[12].sName);

LB1->CommaText = "1012, Spider, Spider, 125, 800, 0, 0, 115, 1, 70, 85, 4, 6, 14, 13, 10, 10, 35, 10, 1, 1, 1, 0, 25, 175, 1000, 1500, 672, 480, 537, 500, 535, 100, 638, 20, 526, 200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 40";
sMonsters[13].sName = LB1->Strings[1];
sMonsters[13].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[13].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[13].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[13].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[13].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[13].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[13].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[13].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[13].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[13].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[13].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[13].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[13].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[13].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[13].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[13].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[13].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[13].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[13].sName);

LB1->CommaText = "1010, Snake, Snake, 115, 850, 0, 0, 56, 1, 75, 90, 4, 6, 20, 11, 10, 10, 35, 10, 1, 1, 1, 0, 20, 133, 900, 1300, 672, 480, 641, 150, 0, 0, 714, 400, 714, 400, 710, 500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 30";
sMonsters[14].sName = LB1->Strings[1];
sMonsters[14].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[14].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[14].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[14].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[14].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[14].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[14].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[14].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[14].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[14].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[14].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[14].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[14].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[14].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[14].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[14].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[14].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[14].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[14].sName);

LB1->CommaText = "1026, MountainSnake, MountainSnake, 130, 900, 0, 0, 63, 1, 80, 125, 4, 6, 20, 40, 10, 10, 35, 45, 1, 1, 1, 0, 20, 133, 1000, 1500, 672, 480, 532, 20, 641, 350, 715, 400, 715, 400, 711, 500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 30";
sMonsters[15].sName = LB1->Strings[1];
sMonsters[15].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[15].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[15].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[15].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[15].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[15].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[15].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[15].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[15].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[15].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[15].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[15].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[15].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[15].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[15].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[15].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[15].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[15].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[15].sName);

LB1->CommaText = "1034, GrassSnake, GrassSnake, 140, 1000, 0, 0, 103, 1, 80, 120, 2, 10, 20, 40, 10, 10, 40, 50, 1, 1, 1, 0, 20, 133, 500, 1100, 672, 480, 716, 400, 716, 400, 712, 500, 676, 500, 660, 1600, 641, 500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 40";
sMonsters[16].sName = LB1->Strings[1];
sMonsters[16].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[16].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[16].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[16].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[16].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[16].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[16].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[16].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[16].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[16].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[16].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[16].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[16].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[16].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[16].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[16].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[16].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[16].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[16].sName);

LB1->CommaText = "1054, Troll, Troll, 65, 2000, 0, 0, 129, 1, 70, 100, 20, 10, 50, 40, 10, 10, 30, 30, 1, 1, 1, 0, 20, 137, 600, 1500, 672, 480, 1199, 2000, 529, 500, 539, 300, 526, 250, 512, 200, 906, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 40";
sMonsters[17].sName = LB1->Strings[1];
sMonsters[17].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[17].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[17].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[17].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[17].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[17].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[17].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[17].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[17].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[17].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[17].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[17].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[17].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[17].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[17].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[17].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[17].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[17].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[17].sName);

LB1->CommaText = "1043, Skeleton, Skeleton, 90, 6800, 0, 0, 1467, 1, 51, 284, 12, 10, 35, 18, 3, 8, 71, 65, 1, 1, 1, 0, 29, 175, 340, 1800, 672, 950, 775, 200, 775, 280, 776, 300, 631, 1000, 778, 250, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 30";
sMonsters[18].sName = LB1->Strings[1];
sMonsters[18].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[18].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[18].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[18].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[18].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[18].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[18].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[18].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[18].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[18].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[18].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[18].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[18].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[18].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[18].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[18].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[18].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[18].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[18].sName);

LB1->CommaText = "1044, LadySkeleton, LadySkeleton, 90, 5500, 0, 0, 957, 2, 73, 146, 31, 8, 20, 25, 15, 8, 43, 48, 1, 1, 1, 0, 29, 175, 650, 750, 672, 700, 775, 200, 775, 280, 776, 300, 631, 1000, 778, 250, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 30";
sMonsters[19].sName = LB1->Strings[1];
sMonsters[19].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[19].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[19].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[19].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[19].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[19].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[19].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[19].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[19].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[19].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[19].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[19].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[19].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[19].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[19].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[19].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[19].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[19].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[19].sName);

LB1->CommaText = "1036, Zombie, Zombie, 110, 12000, 0, 0, 1393, 2, 275, 355, 20, 15, 30, 15, 30, 3, 60, 40, 1, 1, 1, 0, 29, 133, 480, 1872, 672, 900, 631, 1000, 777, 700, 778, 500, 779, 200, 780, 150, 1198, 500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 30";
sMonsters[20].sName = LB1->Strings[1];
sMonsters[20].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[20].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[20].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[20].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[20].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[20].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[20].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[20].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[20].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[20].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[20].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[20].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[20].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[20].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[20].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[20].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[20].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[20].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[20].sName);

LB1->CommaText = "1072, Yeti, Yeti, 100, 8500, 0, 0, 726, 2, 100, 400, 60, 15, 60, 5, 45, 15, 60, 32, 1, 1, 1, 0, 61, 135, 220, 1200, 672, 900, 4023, 700, 4024, 300, 4025, 400, 4027, 142, 4021, 800, 4022, 700, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 30";
sMonsters[21].sName = LB1->Strings[1];
sMonsters[21].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[21].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[21].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[21].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[21].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[21].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[21].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[21].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[21].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[21].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[21].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[21].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[21].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[21].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[21].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[21].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[21].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[21].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[21].sName);

LB1->CommaText = "1062, Terranite, Terranite, 110, 6000, 0, 0, 889, 1, 200, 300, 20, 40, 60, 40, 40, 30, 60, 40, 1, 1, 1, 0, 29, 133, 400, 872, 672, 900, 4001, 600, 4004, 20, 4005, 20, 4007, 20, 763, 500, 640, 500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 30";
sMonsters[22].sName = LB1->Strings[1];
sMonsters[22].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[22].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[22].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[22].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[22].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[22].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[22].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[22].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[22].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[22].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[22].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[22].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[22].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[22].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[22].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[22].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[22].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[22].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[22].sName);


LB1->CommaText = "1022, JackO, JackO, 250, 10000, 0, 0, 1241, 2, 100, 500, 40, 20, 35, 40, 25, 25, 35, 50, 1, 1, 1, 0, 20, 133, 250, 1500, 672, 480, 617, 300, 622, 400, 624, 400, 620, 400, 615, 300, 1203, 400, 1198, 10000, 616, 600, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 30";
sMonsters[23].sName = LB1->Strings[1];
sMonsters[23].iLV    = LB1->Strings[3].ToIntDef(50);
sMonsters[23].iHP    = LB1->Strings[4].ToIntDef(50);
sMonsters[23].iRange = LB1->Strings[8].ToIntDef(50);
sMonsters[23].iATK1  = LB1->Strings[9].ToIntDef(50);
sMonsters[23].iATK2  = LB1->Strings[10].ToIntDef(50);
sMonsters[23].iDEF   = LB1->Strings[11].ToIntDef(50);
sMonsters[23].iMDef  = LB1->Strings[12].ToIntDef(0);
sMonsters[23].iSTR   = LB1->Strings[13].ToIntDef(50);
sMonsters[23].iAGI   = LB1->Strings[14].ToIntDef(50);
sMonsters[23].iVIT   = LB1->Strings[15].ToIntDef(50);
sMonsters[23].iINT   = LB1->Strings[16].ToIntDef(50);
sMonsters[23].iDEX   = LB1->Strings[17].ToIntDef(50);
sMonsters[23].iLUK   = LB1->Strings[18].ToIntDef(50);
sMonsters[23].iElem  = LB1->Strings[23].ToIntDef(20);
sMonsters[23].iMode  = LB1->Strings[24].ToIntDef(129);
sMonsters[23].iSpeed = LB1->Strings[25].ToIntDef(50);
sMonsters[23].iAdelay = LB1->Strings[26].ToIntDef(50);
sMonsters[23].idMotion = LB1->Strings[28].ToIntDef(480);
CB1->Items->Add(sMonsters[23].sName);

    CB1->ItemIndex = 0;
    CB1Change(this);
   }
   __finally{
    delete LB1; // destroy the list object
   }
}

//---------------------------------------------------------------------------
//  Renvoyer la chaine n� de version des propri�t�s d'un fichier
//  ( Limit�e ici: ne renverra pas le n� de construction! )
//---------------------------------------------------------------------------
AnsiString fVerShell (AnsiString Fichier, bool Full)
{   if (!FileExists(Fichier)) return ("");
    DWORD BufSize;
    DWORD Handle  = 0;
    //Handle = BufSize; // Bidon, pour emp�cher un avertissement
    LPDWORD lpdwHandle = &Handle;           // pointer to variable to receive zero
    BufSize = GetFileVersionInfoSize( Fichier.c_str(), lpdwHandle);  // size of buffer
    char *BufData = new char[BufSize];    // pointer to buffer to receive file-version info.
    if (!GetFileVersionInfo( Fichier.c_str(), Handle, BufSize, BufData))
    {   //MessageDlg(fLoc("ERROR_STRING") + " : GetFileVersionInfo", mtError, TMsgDlgButtons() << mbOK, 0);
        delete (BufData); return ("");
    };
    AnsiString VerNumber;// VerNumber= "@"; //=""
    AnsiString LocNumber;// LocNumber= "@"; //=""
    void * lpBuffer;	// Pour recevoir le pointeur sur la chaine de version
    UINT Len; 	    // Pour recevoir la taille de cette chaine
    if (0==VerQueryValue(BufData,(LPTSTR)"\\VarFileInfo\\Translation",&lpBuffer,&Len))
    {   //MessageDlg(fLoc("ERROR_STRING") + " : VerQueryValue 1", mtError, TMsgDlgButtons() << mbOK, 0);
        delete (BufData); return ("");
    }else
    {   LocNumber = IntToHex((int)(unsigned char)((char *)lpBuffer)[1],2);
        LocNumber = LocNumber + IntToHex((int)(unsigned char)((char *)lpBuffer)[0],2);
        LocNumber = LocNumber + IntToHex((int)(unsigned char)((char *)lpBuffer)[3],2);
        LocNumber = LocNumber + IntToHex((int)(unsigned char)((char *)lpBuffer)[2],2);
        LocNumber = "\\StringFileInfo\\" + LocNumber;
        LocNumber = LocNumber + "\\FileVersion";
    };
    if (0==VerQueryValue(BufData, (LPTSTR)LocNumber.c_str(),&lpBuffer,&Len))
    {   //MessageDlg(fLoc("ERROR_STRING") + " : VerQueryValue 2", mtError, TMsgDlgButtons() << mbOK, 0);
        delete (BufData); return ("");
    }else
    {   char Temp[80];
        StrLCopy(Temp, (char *)lpBuffer, Len);
        VerNumber = AnsiString(Temp);
    };
    delete (BufData);
    // Si pas Full; virer le dernier n� ( pour WillyPad: n� de construction...)
    if (!Full) { VerNumber.SetLength(VerNumber.LastDelimiter("\\.:" )-1); };
    return (VerNumber);
}
//----------------------------------------------------------------------------
// Calcul des d�gats moyens recus par une attaque r�ussie d'un Mob
// Calcule en interne la r�duction et l'absorbtion de d�gats.
//----------------------------------------------------------------------------
float fAverage(int iMin, int iMax, int iDef, int iDef2, int iArw)
{   int iDmg, iTot = 0;
    int iCt = 0;
    int iRealMin = iMin;
    int iRealMax = iMax;
    AnsiString s;
    // S�curit�s !
    if (iRealMin > iRealMax)
    {   iRealMax = iMin; iRealMin = iMax;
    }
    if (iArw<0) iArw=0;
    int iRealBow = iRealMax - iArw;
    if (iRealBow<iRealMin) iRealBow = iRealMin;
    // Calculs
    if (iArw<=0)
    {   for (int i=iRealMin; i<=iRealMax; i++)
        {   iDmg = i;
            if (iDmg<1) iDmg = 1;
            // R�duire et absorber du Dmg:
            // D�gats r�duits par la def du Mob
            if (iDef>0) iDmg = (iDmg*(100-iDef))/100;
            if (iDmg<1) iDmg=1;
            // D�gats absorb�s par le Mob
            iDmg = iDmg - (iDef2*8)/10;
            if (iDmg<1) iDmg=1;
            iTot += iDmg;
            iCt ++;
    }   }
    else // Arrow dmg
    {   for (int i=iRealMin; i<=iRealBow; i++)
        {   for (int j=0; j<=iArw; j++)
            {   iDmg = i+j;
                if (iDmg<1) iDmg = 1;
                // R�duire et absorber du Dmg:
                // D�gats r�duits par la def du Mob
                if (iDef>0) iDmg = (iDmg*(100-iDef))/100;
                if (iDmg<1) iDmg=1;
                // D�gats absorb�s par le Mob
                iDmg = iDmg - (iDef2*8)/10;
                if (iDmg<1) iDmg=1;
                iTot += iDmg;
                iCt ++;
    }   }   }
    float f = (iTot*1.0)/(iCt);
    return(f);
}
//---------------------------------------------------------------------------
// Calculer le SkillValue du Focus actuel
//---------------------------------------------------------------------------
void __fastcall TForm1::vCalcSkillValue(TComponent* Owner)
{    int iSkillStat;
     switch (CB2->ItemIndex)
    {   case 1: iSkillStat = iInt;
            break;
        case 2: iSkillStat = iDex;
            break;
        case 3: iSkillStat = iAgi;
            break;
        case 4: iSkillStat = iStr;
            break;
        case 5: iSkillStat = iAgi;
            break;
        default:iSkillStat = 0;
            break;
    }
    int iFocusLvl = UpDownCB2->Position;
    int iSkillValue = iFocusLvl;
    if ((iFocusLvl * 10) - 1 > iSkillStat)
        iSkillValue += ( iSkillStat/ 10);
    else
        iSkillValue *= 2;
    // Enfin, renvoyer la valeur par le Tag du ComboBox
    CB2->Tag = (iSkillValue * iSkillStat) / 10;
    UpDownCB2->Hint ="Adjust your focus level. Actual skill modifier: " +AnsiString(CB2->Tag) + ".";
}

//---------------------------------------------------------------------------
// Ajuster MaxRange selon Mallard's Eye
//---------------------------------------------------------------------------
void __fastcall TForm1::CheckMaxRange(TComponent* Owner, int r)
{   // sd->attackrange += std::min(skill_power(sd, SkillID::AC_OWL) / 60, 3);
    if (CB2->ItemIndex == 2)
    {   iMaxRange = r + (CB2->Tag/60);
    }else
    {   iMaxRange = r;
    }
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{       iStrB = Edit1->Text.ToIntDef(0);
        iAgiB = Edit2->Text.ToIntDef(0);
        iVitB = Edit3->Text.ToIntDef(0);
        iIntB = Edit4->Text.ToIntDef(0);
        iDexB = Edit5->Text.ToIntDef(0);
        iLucB = Edit6->Text.ToIntDef(0);
        iLev = LabeledEditL->Text.ToIntDef(1);
        iSPt = LabeledEditP->Text.ToIntDef(1);
        iStr = LabeledEdit1->Text.ToIntDef(1)+iStrB;
        iAgi = LabeledEdit2->Text.ToIntDef(1)+iAgiB;
        iVit = LabeledEdit3->Text.ToIntDef(1)+iVitB;
        iInt = LabeledEdit4->Text.ToIntDef(1)+iIntB;
        iDex = LabeledEdit5->Text.ToIntDef(1)+iDexB;
        iLuc = LabeledEdit6->Text.ToIntDef(1)+iLucB;
        iArmor = 0;
        iDmg = 0;
        iDmgView = 1; // defaut armes 1Hd
        iMagic = 0;
        iCritCh = 0;
        iArrowDmg = 0;
        iHitDelay = 1000;
        sRangeDmg[0].iAtk1= -1;  // Marqueur fin partie utilis�e
        Application->OnHint = DisplayHint;
        // StatusBar1->Panels->Items[1]->Text = "Snoopy81";
        StatusBar1->Panels->Items[1]->Text = "v "+fVerShell (ParamStr(0), false);
        Label7->Caption = "StatsMP by Snoopy81 - 2017 - Stats and DPS computer for The Mana World.";
        Player_Select(this);
        LoadDefautMobs(this);
        Application->OnShowHint = DoShowHint;             
        CB2->ItemIndex = 0;   // Pas de focus
        CB2Select(this);
}
//----------------------------------------------------------------------------
// Passer les HINTs sur plusieurs lignes
//----------------------------------------------------------------------------
void __fastcall TForm1::DoShowHint(System::AnsiString &HintStr, bool &CanShow, THintInfo &HintInfo)
{    HintInfo.HintMaxWidth = 120; // Hint text word wraps if width is greater than 120
} 
//----------------------------------------------------------------------------
// Copie les astuces vers la barre d'�t�t
//----------------------------------------------------------------------------
void __fastcall TForm1::DisplayHint(TObject *Sender)
{   StatusBar1->Panels->Items[0]->Text = GetLongHint(Application->Hint);
    AnsiString s = GetLongHint(Application->Hint);
    //Label7->Caption = GetShortHint(LabelS4->Hint);
    if (RadioButton3->Checked || RadioButton5->Checked)
    {   if (s == "%%$$##W" )
        {   LabeledEditO5->Color = clHighlight;
            LabeledEditO5->Text = "+ "+ AnsiString(LabelS3a->Tag)+" ms";
            StatusBar1->Panels->Items[0]->Text = GetShortHint(LabelS3->Hint);
        }else if (s == "%%$$##LS" )
        {   LabeledEditO5->Color = clHighlight;
            LabeledEditO5->Text = "+ "+ AnsiString(LabelS4->Tag)+" ms";
            StatusBar1->Panels->Items[0]->Text = GetShortHint(LabelS6->Hint);
        }else
        {   LabeledEditO5->Color = clBtnFace;
            LabeledEditO5->Text = AnsiString(iHitDelay)+"ms (x2)";
    }   }
}
//---------------------------------------------------------------------------
//      Tout changement d'une des stats
//---------------------------------------------------------------------------
void __fastcall TForm1::Stat_OnChange(TObject *Sender)
{ //   Stat by one can be calculated as [(BaseOfStat - 1)/ 10] + 2.
    iStr = LabeledEdit1->Text.ToIntDef(1);
    int iCost = (iStr-1)/10 +2;
    Label1->Tag = iCost;
    Label1->Caption = "(+" + AnsiString(iCost)+")";

    iAgi = LabeledEdit2->Text.ToIntDef(1);
    iCost = (iAgi-1)/10 +2;
    Label2->Tag = iCost;
    Label2->Caption = "(+" + AnsiString(iCost)+")";

    iVit = LabeledEdit3->Text.ToIntDef(1);
    iCost = (iVit-1)/10 +2;
    Label3->Tag = iCost;
    Label3->Caption = "(+" + AnsiString(iCost)+")";

    iInt = LabeledEdit4->Text.ToIntDef(1);
    iCost = (iInt-1)/10 +2;
    Label4->Tag = iCost;
    Label4->Caption = "(+" + AnsiString(iCost)+")";

    iDex = LabeledEdit5->Text.ToIntDef(1);
    iCost = (iDex-1)/10 +2;
    Label5->Tag = iCost;
    Label5->Caption = "(+" + AnsiString(iCost)+")";

    iLuc = LabeledEdit6->Text.ToIntDef(1);
    iCost = (iLuc-1)/10 +2;
    Label6->Tag = iCost;
    Label6->Caption = "(+" + AnsiString(iCost)+")";

    this->Anything_Changed(this);
}
//---------------------------------------------------------------------------
//      Changement dans une des cases de bonus
//---------------------------------------------------------------------------
void __fastcall TForm1::Bonus_OnChange(TObject *Sender)
{       iStrB = Edit1->Text.ToIntDef(0);
        if (iStrB==0)
        {   Edit1->Font->Color = clWindowText;
        }else
        {   Edit1->Font->Color = clRed;
        }

        iAgiB = Edit2->Text.ToIntDef(0);
        if (iAgiB==0)
        {   Edit2->Font->Color = clWindowText;
        }else
        {   Edit2->Font->Color = clRed;
        }

        iVitB = Edit3->Text.ToIntDef(0);
        if (iVitB==0)
        {   Edit3->Font->Color = clWindowText;
        }else
        {   Edit3->Font->Color = clRed;
        }

        iIntB = Edit4->Text.ToIntDef(0);
        if (iIntB==0)
        {   Edit4->Font->Color = clWindowText;
        }else
        {   Edit4->Font->Color = clRed;
        }

        iDexB = Edit5->Text.ToIntDef(0);
        if (iDexB==0)
        {   Edit5->Font->Color = clWindowText;
        }else
        {   Edit5->Font->Color = clRed;
        }
        
        iLucB = Edit6->Text.ToIntDef(0);
        if (iLucB==0)
        {   Edit6->Font->Color = clWindowText;
        }else
        {   Edit6->Font->Color = clRed;
        }

        this->Anything_Changed(this);
}
//---------------------------------------------------------------------------
//      Changement dans le niveau choisi
//---------------------------------------------------------------------------
void __fastcall TForm1::Level_OnChange(TObject *Sender)
{       iLev = LabeledEditL->Text.ToIntDef(1);
        // Status Points du niveau suivant en Hint
        int i = (iLev+1+2)/4 + 3;
        AnsiString s = "Select your level to compute status points.";
        s = s + " Next level: +" + AnsiString(i);
        LabeledEditL->Hint = s;
        this->Anything_Changed(this);
}
//---------------------------------------------------------------------------
//      Quelque chose � chang�: tout remettre en place
//---------------------------------------------------------------------------
void __fastcall TForm1::Anything_Changed(TObject *Sender)
{       //  Mise � jour des variables globales
        iLev = LabeledEditL->Text.ToIntDef(1);
        iSPt = LabeledEditP->Text.ToIntDef(1);
        iStr = LabeledEdit1->Text.ToIntDef(1)+iStrB;
        iAgi = LabeledEdit2->Text.ToIntDef(1)+iAgiB;
        iVit = LabeledEdit3->Text.ToIntDef(1)+iVitB;
        iInt = LabeledEdit4->Text.ToIntDef(1)+iIntB;
        iDex = LabeledEdit5->Text.ToIntDef(1)+iDexB;
        iLuc = LabeledEdit6->Text.ToIntDef(1)+iLucB;
        // Calcul des Status points.
        int b = 0;
        for(int i=2;i<=LabeledEdit1->Text.ToIntDef(1);i++) b += (i-2)/10 +2;
        for(int i=2;i<=LabeledEdit2->Text.ToIntDef(1);i++) b += (i-2)/10 +2;
        for(int i=2;i<=LabeledEdit3->Text.ToIntDef(1);i++) b += (i-2)/10 +2;
        for(int i=2;i<=LabeledEdit4->Text.ToIntDef(1);i++) b += (i-2)/10 +2;
        for(int i=2;i<=LabeledEdit5->Text.ToIntDef(1);i++) b += (i-2)/10 +2;
        for(int i=2;i<=LabeledEdit6->Text.ToIntDef(1);i++) b += (i-2)/10 +2;

        // Calcul Status Points
        iSPt = 45 - b;
        for (int i = 1; i<=iLev; i++)
        {    iSPt = iSPt + (i+2)/4 + 3;
        }
        LabeledEditP->Text = AnsiString(iSPt);
        if (iSPt>=0)
        {   LabeledEditP->Font->Color = clWindowText;
        }else
        {   LabeledEditP->Font->Color = clRed;
        }


        // Mise � jour de la valeur du bonus des Skills FOCUS
        switch (CB2->ItemIndex)
        {   case 1: iSkillStat = iInt; break;   //TMW_ASTRAL_SOUL
            case 2: iSkillStat = iDex; break;   //AC_OWL
            case 3: iSkillStat = iAgi; break;   //TMW_SPEED
            case 4: iSkillStat = iStr; break;   //TMW_RAGING
            case 5: iSkillStat = iAgi; break;   //TMW_BRAWLING
            default:iSkillStat = 0;    break;
            
        } 
        vCalcSkillValue(this);
        CheckMaxRange(this, Button1->Tag);
        if (RadioButton1->Checked == true)       // Preset "Nothing"
        {   if (CB2->ItemIndex == 5) iDmg = CB2->Tag/3;
            else                     iDmg = 0;
        }        

        // Calcul MP, HP & regen
        int iHP = iLev*5 + 35;
        iHP = iHP*(iVit+100)/100;
        int iHPr = 1 + iVit/5 + iHP/200;
        LabeledEditHP->Text = AnsiString(iHP)+" / "+AnsiString(iHPr);
        int iMP = iLev +10;
        iMP = iMP*(iInt+100)/100;
        int iMPr = iInt/6 + iMP/100 + 1;
        if (iInt>120) iMPr = (iInt-120)/2 + iMP/100 + 21;
        LabeledEditMP->Text = AnsiString(iMP)+" / "+AnsiString(iMPr);

        // Max Weigth
        float fWei = (210.0+iStr*3.0)/10.0;
        AnsiString s=FormatFloat("0.0",fWei);
        LabeledEditW->Text = AnsiString(s)+" KG";

        // Attack TAB:
        // Hit chance:
        int iHit;
        if (RadioButton4->Checked)  // A l'arc
        {   iHit = iLev + iDex;
            // La distance r�duit le toucher a l'arc selon Mallard's Eye
            int malus_dist;
            if (CB2->ItemIndex == 2) malus_dist = 4 - int(CB2->Tag /75);
            else                     malus_dist = 4;
            if (malus_dist < 0) malus_dist = 0;
            iHit-= (malus_dist * (malus_dist + 1));
            // Mallard's Eye am�liore le toucher � l'arc
            if (CB2->ItemIndex == 2) 
            {   iHit += CB2->Tag / 10;   // 20 for 200
            }
        }else                    // Au C�C
        {    iHit = iLev + iDex;
            // Brawling am�liore le toucher
            if (CB2->ItemIndex == 5)  iHit += CB2->Tag/16;
        }
        if (iHit <0) LabeledEditO6->Text = AnsiString(iHit);
        else         LabeledEditO6->Text = "+"+AnsiString(iHit);

        // CaC Attack
        int iCAtk = iStr/10;
        iCAtk = iCAtk*iCAtk + iStr;
        iCAtk = iCAtk + iDex/5;
        iCAtk = iCAtk + iLuc/5;
        // NB: TMW_BRAWLING pris en compte comme une arme
        if ((RadioButton4->Checked))
        {   LabeledEditO1->Text = AnsiString(iCAtk);
            LabeledEditO1->Font->Color = clWindowText;
        }else
        {   LabeledEditO1->Text = AnsiString(iCAtk + iDmg) +" [ "+ AnsiString(iCAtk)
                                +" + "+AnsiString(iDmg)+" ]";
        }


        // Range Attack
        int iRAtk = iDex/10;
        iRAtk = iRAtk*iRAtk + iDex;
        iRAtk = iRAtk + iStr/5;
        iRAtk = iRAtk + iLuc/5;
        if (RadioButton4->Checked)
        {   LabeledEditO2->Text = AnsiString(iRAtk + iDmg)+" [ "+ AnsiString(iRAtk)
                                +" + "+AnsiString(iDmg)+" ] (+"+AnsiString(iArrowDmg)+")";
        }else
        {   LabeledEditO2->Text = AnsiString(iRAtk);
        }

        // Magic Attack avec ou sans ASTRAL SOUL
        iMAtk1 = iInt/5;
        iMAtk1 = iLev + iInt + iMAtk1*iMAtk1;  // MAtk1: SANS le bonus equipement
        iMAtk2 = iInt/5;
        iMAtk2 = iLev + iInt + iMAtk2*iMAtk2;  // MAtk2: AVEC le bonus equipement
        iMAtk2 += iMagic;
        if (iMAtk1 > 200)   // Prise en compte ASTRAL SOUL,
        {   int iBonus = iMAtk1-200; // Pour calculer le b�n�fice ajust� par ASTRAL SOUL
            iMAtk1 = 200 + iBonus/8;
            if (CB2->ItemIndex == 1) iMAtk1 += (3*iBonus*CB2->Tag)/512; // Astral Soul 9
        }
        if (iMAtk2 > 200)   // Prise en compte ASTRAL SOUL
        {   int iBonus = iMAtk2-200;
            iMAtk2 = 200 + iBonus/8;
            if (CB2->ItemIndex == 1) iMAtk2 += (3*iBonus*CB2->Tag)/512; // Astral Soul 9
        }
        //s = AnsiString(iMAtk2)+")";
        LabeledEditO3->Hint = "Magic score, based on intelligence.";// (Dmg: "+s;
        if ((RadioButton3->Checked)||(RadioButton5->Checked))
        {   LabeledEditO3->Text =  AnsiString(iMAtk2)+" [ "+ AnsiString(iMAtk1)
                                +" + "+AnsiString(iMAtk2-iMAtk1)+" ]";
            LabeledEditO3->Font->Color = clBlue;
        }else if (RadioButton1->Checked)
        {   LabeledEditO3->Text = AnsiString(iMAtk2);
            LabeledEditO3->Font->Color = clWindowText;
        }else
        {   if ((iMAtk2) <= 0)
            {   LabeledEditO3->Text = AnsiString("0");
                LabeledEditO3->Hint = "Magic score, nullifyed by heavy armor.";
            }else
            {    LabeledEditO3->Text = AnsiString(iMAtk2);
            }
            LabeledEditO3->Font->Color = clRed;
        }

        // Critical chance
        iCritCh = 10+(iLuc*3);       // Impossible � v�rifier cot� serveur, pourtant affich� par le client
        if (!RadioButton4->Checked)  // arme C�C
        {   iCritCh = iCritCh + (iDex*3/2);
            // Appliquer Raging 9 focus, uniquement armes C�C
            // Techniquement ca devrait aussi marcher � l'arc � port�e<=2
            if (CB2->ItemIndex == 4) // TMW_RAGING
            {   iCritCh +=  iCritCh * CB2->Tag/100;
        }   }
        s=FormatFloat("0.0",((float)iCritCh)/10.0);
        LabeledEditO4->Text = AnsiString(s)+"%";

        // Weapon Speed      1% <==> 2.5 Agil ou 8 Dext
        float fSpeed1 = iDex*2 + iAgi*8;
        int iSpeed;
        if ((RadioButton1->Checked)||(iDmgView==10))// nu ou Staff = 650
        {   iSpeed = 651- (fSpeed1)*0.3255;
        }else if (iDmgView==3)                      // Scite ou Setzer = 600
        {   iSpeed = 601- (fSpeed1)*0.3005;
        }else if (iDmgView==11)                     // Bow, Sling = 900
        {   iSpeed = 901- (fSpeed1)*0.4505;
        }else // if (iDmgView==1)                   // 1 Hand Weapon = 700
        {   iSpeed = 701- (fSpeed1)*0.3505;
        }
        // Appliquer Speed 9 focus power
        if (CB2->ItemIndex == 3)
        {   int iRate = 100 - CB2->Tag/10;
            iSpeed = (iSpeed * iRate)/100;
        }
        if (iSpeed < 199) iSpeed = 199;  // Minima 199 ms
        LabeledEditO5->Text = AnsiString(iSpeed)+"ms (x2)";
        iHitDelay = iSpeed;


        // Raw DPS
        if (RadioButton4->Checked)
        {   fRawDPS = ((iRAtk+iDmg)*1000.0)/iSpeed;   // Ranged raw DPS
        }else
        {   fRawDPS = ((iCAtk+iDmg)*1000.0)/iSpeed;   // CAC raw DPS
        }
        LabeledEditDPS->Text = FormatFloat("0",fRawDPS);


        // Defense
        int iDef = iVit;
        // Appliquer Raging 9 Malus
        if (CB2->ItemIndex == 4) // TMW_RAGING
        {   iDef= (iDef * 256) / (256 + CB2->Tag);
        }
        if (iArmor<=0)
        {   LabeledEditD1->Text = "None";
            LabeledEditD7->Text = "-"+AnsiString(floor(iDef*0.8))+" dmg";
        }else
        {   LabeledEditD1->Text = AnsiString(iArmor)+" %";
            LabeledEditD7->Text = "-"+AnsiString(floor(iDef*0.8))+" dmg";
        }
        LabelDmg->Caption = "Defense: "+AnsiString(iArmor+iDef)+"="
                        +AnsiString(iArmor)+"+"+AnsiString(iDef);


        // Evasion  et Lucky dodge
        int iEvA = iAgi + iLev;
        if (CB2->ItemIndex == 3)  iEvA += CB2->Tag/8; // Bonus pour Focus Speed 9
        if (CB2->ItemIndex == 5)  iEvA += CB2->Tag/8; // Bonus pour Focus Brawling 9
        LabeledEditD3->Text = "-"+AnsiString(iEvA);
        float fEvL = (10.0 + iLuc)/10;
        if (CB2->ItemIndex == 3)  fEvL += CB2->Tag/8; // Bonus pour Focus Speed 9
        if (CB2->ItemIndex == 5)  fEvL += CB2->Tag/8; // Bonus pour Focus Brawling 9
        s=FormatFloat("0.0",fEvL);
        LabeledEditD2->Text = s +" %";


        // Magical Defense  Magical defense, based on intelligence. (Hidden on vitality)
        s=AnsiString(iInt)+" ("+AnsiString(iVit/2)+")";
        LabeledEditD4->Text = s;   

        // Poison resistance   3 + (Vitality + (Luck / 3)) % chance of resisting poison.
        int iPR = 3 + iVit + (iLuc/3);
        if (iPR>100) iPR = 100;
        LabeledEditD5->Text = AnsiString(iPR)+" %";

        // Critical Defence
        float fCritDef = (iLuc *3.0)/10.0;
        s=FormatFloat("0.0",fCritDef);
        LabeledEditD6->Text = AnsiString(s)+" %";

        this->CB1Change(this);    
//    LabelTest->Caption = AnsiString((float)fSpeed1);

}
//---------------------------------------------------------------------------
//      Reset stats
//---------------------------------------------------------------------------
void __fastcall TForm1::Reset_Stats(TObject *Sender)
{       // Reset Edits
        LabeledEdit1->Text = AnsiString(1);
        LabeledEdit2->Text = AnsiString(1);
        LabeledEdit3->Text = AnsiString(1);
        LabeledEdit4->Text = AnsiString(1);
        LabeledEdit5->Text = AnsiString(1);
        LabeledEdit6->Text = AnsiString(1);
        LabeledEditL->Text = AnsiString(1);
        this->Stat_OnChange(this);
        Edit1->Text = AnsiString(0);
        Edit2->Text = AnsiString(0);
        Edit3->Text = AnsiString(0);
        Edit4->Text = AnsiString(0);
        Edit5->Text = AnsiString(0);
        Edit6->Text = AnsiString(0);
        this->Bonus_OnChange(this);
}
//---------------------------------------------------------------------------
// Choix d'un �quipement par d�faut.
//---------------------------------------------------------------------------
void __fastcall TForm1::Player_Select(TObject *Sender)
{   // Virer les bonus caract�ristique des armes ou armures
    if (LabelFA->Tag == 3)
    {   UpDown15->Position -= 3;
        LabelFA->Tag = 0;
        LabelFA->Hide();
    }
    if (LabelIG->Tag == 1)
    {   UpDown16->Position -= 1;
        LabelIG->Tag = 0;
        LabelIG->Hide();
    }
    if (LabelRK->Tag == 3)
    {   UpDown13->Position -=3;
        LabelRK->Tag = 0;
        LabelRK->Hide();
    }
    RadioButton1->Font->Color = clWindowText;
    RadioButton2->Font->Color = clWindowText;
    RadioButton3->Font->Color = clWindowText;
    RadioButton4->Font->Color = clWindowText;
    RadioButton5->Font->Color = clWindowText;
    RadioButton6->Font->Color = clWindowText;
    RadioButton7->Font->Color = clWindowText;
    LabeledEditO1->Font->Color = clWindowText;
    LabeledEditO2->Font->Color = clWindowText;
    LabeledEditO3->Font->Color = clWindowText;
    LabeledEditO4->Font->Color = clWindowText;
    LabeledEditO5->Font->Color = clWindowText;
    LabeledEditO6->Font->Color = clWindowText;
    LabeledEditD1->Font->Color = clWindowText;
    LabeledEditD2->Font->Color = clWindowText;
    LabeledEditD3->Font->Color = clWindowText;
    LabeledEditD4->Font->Color = clWindowText;
    LabeledEditD5->Font->Color = clWindowText;
    LabeledEditD6->Font->Color = clWindowText;
    GroupBox4->PopupMenu = 0;
    iArrowDmg = 0;
    iMaxRange = 0;
    Button1->Tag = 0;

    if (RadioButton7->Checked)            // Tank
    {   RadioButton7->Font->Color = clPurple;
        LabeledEditO1->Font->Color = clPurple;
        LabeledEditD1->Font->Color = clPurple;
        LabeledEditO5->Font->Color = clPurple; 
        LabelS1->Font->Color = clPurple;
        iArmor = 80;
        iDmg = 110;
        iDmgView = 3;   // Scyte, Setzer speed
        Setzer1->Checked = true;
        Button1->Caption = Setzer1->Caption;
        Button1->Enabled = true;
        Button1->Width = 121;
        Label10->Caption = "Weapon:";
        iMagic = -470;
        LabeledEditO1->Width = 105;
        LabeledEditO2->Left  = 128;
        LabeledEditO2->Width = 49;
        LabeledEditO3->Left  = 192;
        LabeledEditO3->Width = 49;           
        CB2->ItemIndex = 0;   // Pas de focus            
        CB2Select(this);
     }else if (RadioButton6->Checked)      // Warrior
    {   RadioButton6->Font->Color = clPurple;
        LabeledEditO1->Font->Color = clPurple;
        LabeledEditD1->Font->Color = clPurple;
        LabeledEditO5->Font->Color = clPurple;
        LabelS1->Font->Color = clPurple;
        iArmor = 67;
        iDmg = 100;
        iDmgView = 1;  // 1Hd weapon speed
        ShortSword1->Checked = true;
        Button1->Caption = ShortSword1->Caption;
        Button1->Enabled = true;
        Button1->Width = 121;
        Label10->Caption = "Weapon:";
        iMagic = -285;
        LabeledEditO1->Width = 105;
        LabeledEditO2->Left  = 128;
        LabeledEditO2->Width = 49;
        LabeledEditO3->Left  = 192;
        LabeledEditO3->Width = 49;
        CB2->ItemIndex = 0;   // Pas de focus   
        CB2Select(this);
    }else if (RadioButton5->Checked)      // Wizard
    {   RadioButton5->Font->Color = clBlue;
        LabeledEditO3->Font->Color = clBlue;
        LabeledEditD1->Font->Color = clBlue;
        LabelS1->Font->Color = clBlue;
        iArmor = 16;
        iDmg = 50;
        iDmgView = 10; // No Weapon, staff speed
        iMagic = 76;
        Button1->Hint = "Wooden Staff";
        Button1->Caption = "Staff";
        Button1->Enabled = false;
        Button1->Width = 57;
        Label10->Caption = "             M.Atk Bonus:";
        LabeledEditO1->Width = 81;      //+24
        LabeledEditO2->Left  = 104;
        LabeledEditO2->Width = 33;
        LabeledEditO3->Left  = 152;
        LabeledEditO3->Width = 89;
        CB2->ItemIndex = 1;            // Astral Soul
        CB2Select(this);
    }else if (RadioButton4->Checked)      // Archer
    {   RadioButton4->Font->Color = clGreen;
        LabeledEditO2->Font->Color = clGreen;
        LabeledEditO4->Font->Color = clGreen;
        LabeledEditD1->Font->Color = clGreen;
        LabeledEditO5->Font->Color = clGreen;
        LabelS1->Font->Color = clGreen;
        iArmor = 32;
        iDmg = 70;
        iDmgView = 11; // Range weapon speed
        iMagic = -61;
        iArrowDmg = 20;
        //iMaxRange = 5;
        Button1->Tag = 5;
        CheckMaxRange(this, 5);
        MenuItem15->Checked = true;
        Button1->Caption = MenuItem15->Caption;
        Button1->Enabled = true;
        Button1->Width = 121;
        Label10->Caption = "Weapon:";
        // plus bonus 3 en Dext
        UpDown15->Position += 3;
        LabelFA->Tag = 3;
        LabelFA->Show();
        LabeledEditO1->Width = 49;
        LabeledEditO2->Left  = 72;
        LabeledEditO2->Width = 105;
        LabeledEditO3->Left  = 192;
        LabeledEditO3->Width = 49;
        CB2->ItemIndex = 0;   // Pas de focus
        CB2Select(this);
    }else if (RadioButton3->Checked)      // Mage
    {   RadioButton3->Font->Color = clBlue;
        LabeledEditO3->Font->Color = clBlue;
        LabeledEditD1->Font->Color = clBlue;
        LabelS1->Font->Color = clBlue;
        iArmor = 10;
        iDmg = 1;
        iDmgView = 1;  // 1Hd weapon speed
        iMagic = 7;
        Button1->Caption = "Wand";
        Button1->Enabled = false;
        Button1->Width = 57;
        Label10->Caption = "             M.Atk Bonus:";
        LabeledEditO1->Width = 81;      //+24
        LabeledEditO2->Left  = 104;
        LabeledEditO2->Width = 33;
        LabeledEditO3->Left  = 152;
        LabeledEditO3->Width = 89;
        CB2->ItemIndex = 1;            // Astral Soul     
        CB2Select(this);
    }else if (RadioButton2->Checked)      // Noob
    {   RadioButton2->Font->Color = clPurple;
        LabeledEditO1->Font->Color = clPurple;
        LabeledEditD1->Font->Color = clPurple;
        LabeledEditO5->Font->Color = clPurple;
        LabelS1->Font->Color = clPurple;
        iArmor = 18;
        iDmg = 75;
        iDmgView = 3;  // Scyte, Setzer speed
        Scyte1->Checked = true;
        Button1->Caption = Scyte1->Caption;
        Button1->Enabled = true;
        Button1->Width = 121;
        Label10->Caption = "Weapon:";
        iMagic = -46;
        LabeledEditO1->Width = 105;
        LabeledEditO2->Left  = 128;
        LabeledEditO2->Width = 49;
        LabeledEditO3->Left  = 192;
        LabeledEditO3->Width = 49;
        CB2->ItemIndex = 0;   // Pas de focus     
        CB2Select(this);
    }else  // RadioBotton1 ou aucun.    // Nude
    {   RadioButton7->Font->Color = clWindowText;
        LabelS1->Font->Color = clWindowText;
        iArmor = 0;
        iDmg = 0;
        iDmgView = 10;  // No Weapon seep
        iMagic = 0;
     /*   if (CB2->ItemIndex != 5) // TMW_BRAWLING
        { CB2->ItemIndex = 0;   // Pas de focus
        }     */
        Button1->Caption = "None";
        Button1->Enabled = false;
        Button1->Width = 121;
        Label10->Caption = "Weapon:";
        LabeledEditO1->Width = 105;
        LabeledEditO2->Left  = 128;
        LabeledEditO2->Width = 49;
        LabeledEditO3->Left  = 192;
        LabeledEditO3->Width = 49;
        CB2->ItemIndex = 0;   // Pas de focus    
        CB2Select(this);
    }
    LabelS2->Font->Color = LabelS1->Font->Color;
    LabelS3->Font->Color = LabelS1->Font->Color;
    LabelS3a->Font->Color = LabelS1->Font->Color;
    LabelS4->Font->Color = LabelS1->Font->Color;
    LabelS5->Font->Color = LabelS1->Font->Color;
    LabelS6->Font->Color = LabelS1->Font->Color;
    LabelS6->Visible = RadioButton3->Checked || RadioButton4->Checked || RadioButton5->Checked;
    LabeledEditO2->EditLabel->Width = LabeledEditO2->Width;
    Timer2->Enabled = RadioButton4->Checked;

    UpDownAR->Position = iArmor;
    UpDownMB->Position = iMagic;
    this->Bonus_OnChange(this);
}
//---------------------------------------------------------------------------
// AJUSTER L'ARMURE MANUELLEMENT
//---------------------------------------------------------------------------
void __fastcall TForm1::TrackBar1Change(TObject *Sender)
{   //TrackBar1->SelEnd = TrackBar1->Position;
    iArmor = UpDownAR->Position;
    Timer1->Enabled = true;
    LabeledEditD1->Color = clHighlight;
    this->Bonus_OnChange(this);
}
//---------------------------------------------------------------------------
// AJUSTER LE BONUS MAGIC POWER MANUELLEMENT
//---------------------------------------------------------------------------
void __fastcall TForm1::EditMBChange(TObject *Sender)
{  //
    iMagic = UpDownMB->Position;
    Timer1->Enabled = true;
    LabeledEditO3->Color = clHighlight; 
    this->Bonus_OnChange(this);
}
//---------------------------------------------------------------------------
// faire clignoter Dmg reduc
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{    Timer1->Enabled = false;
     LabeledEditD1->Color = clBtnFace;
     LabeledEditD3->Color = clBtnFace;
     LabeledEditD7->Color = clBtnFace;
     LabeledEditO1->Color = clBtnFace;
     LabeledEditO2->Color = clBtnFace;
     LabeledEditO3->Color = clBtnFace;
     LabeledEditO4->Color = clBtnFace;
     LabeledEditO5->Color = clBtnFace;
}
//---------------------------------------------------------------------------
// Ne pas garder le focus, ca change en Bleu.
//---------------------------------------------------------------------------
void __fastcall TForm1::CB1Select(TObject *Sender)
{  // LabeledEditL->SetFocus();
}
//---------------------------------------------------------------------------
// Select an other mob
//---------------------------------------------------------------------------
void __fastcall TForm1::CB1Change(TObject *Sender)
{   int p = CB1->ItemIndex;
    if (p == -1) return;
    //this->Anything_Changed(this);
    // Extraire des variables pour simplifier la suite.
    int mHit, mFlee, mFle2, mCri, mDef, mDef2, mAtk1, mAtk2, mHP, mLev, mStr, mMode, mMDef, mElem, mRange;
    mHit  = 80 + sMonsters[p].iLV + sMonsters[p].iDEX;
    mFlee = sMonsters[p].iLV + sMonsters[p].iAGI;
    mFle2 = 10 + sMonsters[p].iLUK;     // sur 1000
    mCri  = 10 + ((sMonsters[p].iLUK*10)/3); // sur 1000
    mDef  = sMonsters[p].iDEF;
    mDef2 = sMonsters[p].iVIT;
    mAtk1 = sMonsters[p].iATK1;
    mAtk2 = sMonsters[p].iATK2;
    mHP   = sMonsters[p].iHP;
    mLev  = sMonsters[p].iLV;
    mStr  = sMonsters[p].iSTR;
    mMode = sMonsters[p].iMode;
    mMDef = sMonsters[p].iMDef;
    mElem = sMonsters[p].iElem;// - 20;
    mRange = sMonsters[p].iRange;
    // NB: Le serveur avait du �tre con�u avec:
    //     - r�sistances �l�mentaires des mobs  (class Elements)
    //     - El�ment d'attaque du mob           (struct LevelElement)
    //     Le syst�me n'a pas �t� finalis�, du coup l'�l�ment d'attaque
    //     est en fait utilis� comme r�sistance �l�mentaire par TMW
    int mElemType, mElemLevel;
    mElemType  = mElem % 10;
    mElemLevel = mElem / 10;
    /*
// Elements
ELT_NEUTRAL     0       1
ELT_WATER       1       2
ELT_EARTH       2       4
ELT_FIRE        3       8
ELT_WIND        4       16
ELT_POISON      5       32
ELT_SHADOW      6       64
ELT_HOLY        7       128
ELT_GHOST       8       256
ELT_UNDEAD      9       512


enum class Element : uint8_t
{
    neutral = 0,
    water   = 1,
    earth   = 2,
    fire    = 3,
    wind    = 4,
    poison  = 5,
    _holy   = 6,
    dark    = 7,
    _spirit = 8,
    undead  = 9,

    COUNT   = 10,
};
struct LevelElement
{
    uint8_t level;
    Element element;

    static
    LevelElement unpack(int packed)
    {
        LevelElement le;
        le.element = static_cast<Element>(packed % 10);
        le.level = packed / 10;
        return le;
    }
    int pack() const
    {
        return level * 10 + static_cast<uint8_t>(element);
    }
};

namespace e
{
// Ici c'est un tout autre truc on dirait:
// Pas confondre Element et Elements
enum class Elements : uint16_t
{
    ZERO    = 0x0000,
    neutral = 1 << 0,
    water   = 1 << 1,
    earth   = 1 << 2,
    fire    = 1 << 3,
    wind    = 1 << 4,
    poison  = 1 << 5,
    _holy   = 1 << 6,
    dark    = 1 << 7,
    _spirit = 1 << 8,
    undead  = 1 << 9,
};
ENUM_BITWISE_OPERATORS(Elements)
    */

    int jHit, jFlee, jFle2, jCri, jDef, jDef2, jDef3, jDef5, jDef23, jDef25;
    jHit  = 80 + iLev + iDex;
    jFlee = iLev + iAgi;
    jFle2 = 10 + iLuc;                  // sur 1000
    jCri  = iCritCh;                    // sur 1000
    jDef  = iArmor;
    jDef2 = iVit;
    jDef3 = iArmor *0.95;
    jDef23 = iVit  *0.95;
    jDef5 = iArmor *0.85;
    jDef25 = iVit  *0.85;
    // Appliquer bonus et malus pour Raging 9
    if (CB2->ItemIndex == 4) // TMW_RAGING
    {  jDef2 = (jDef2 * 256) / (256 + CB2->Tag);
       jDef23 = (jDef23 * 256) / (256 + CB2->Tag);
       jDef25 = (jDef25 * 256) / (256 + CB2->Tag);
       jCri += jCri * CB2->Tag/100;
    }
    // Appliquer bonus pour Brawling 9
    if (CB2->ItemIndex == 5) //TMW_BRAWLING
    {  jFlee += CB2->Tag/8;
       jFle2 += CB2->Tag/8;
    }
    // Appliquer bonus pour Speed 9
    if (CB2->ItemIndex == 3)
    {  jFlee += CB2->Tag/8;
       jFle2 += CB2->Tag/8;
    }

    // Afficher les donn�es du Mob
    AnsiString s;
    s = " Lvl "+AnsiString(mLev)+", "+AnsiString(mHP)+"HP";
    LabelS11->Caption = s;
    s= " Dmg "+AnsiString(mAtk1)+"/"+AnsiString(mAtk2)+", "+AnsiString(sMonsters[p].iAdelay)+"ms";
    LabelS10->Caption = s;
    switch (mMode){   //1: Can fight, 2:Pickup, 4: Aggro, 8:Assist
    case 0:
    case 128: LabelS13->Caption = " Static"; break;
    case 129:
    case 161: //Santa Slime x32
    case 131: LabelS13->Caption = " Peacefull"; break;
    case 133: // Aggro
    case 135: // Aggro + pickup
    case 141: // Aggro + Assist
    case 165: // Aggro + x32
    case 181: // Luvia +x32 + x16
    case 175: LabelS13->Caption = " Hostile"; break;   // Aggro + Assists + pickup +x32
    case 137:
    case 139: LabelS13->Caption = " Supporter"; break;
    default:  LabelS13->Caption = " Unknown";  break;
    }
    LabelS13->Caption = LabelS13->Caption + ", Mv " + AnsiString(sMonsters[p].iSpeed)+"ms";

    // Hints: 1-Stats du mob
    s="Defense: "+AnsiString(mDef)
     +"    Range: "+ AnsiString(mRange)
     +"    Str: "+AnsiString(mStr)
     +", Agi: "+AnsiString(sMonsters[p].iAGI)+", Vit: "+AnsiString(sMonsters[p].iVIT)
     +", Int: "+AnsiString(sMonsters[p].iINT)+", Dex: "+AnsiString(sMonsters[p].iDEX)
     +", Luk: "+AnsiString(sMonsters[p].iLUK)+".";
    LabelS11->Hint = s;
    // Hints: 2-Vitesses du mob
    s="Move: "    + AnsiString(sMonsters[p].iSpeed)
     +"ms, Atk: " + AnsiString(sMonsters[p].iAdelay)
     +"ms, Def mezz: "+ AnsiString(sMonsters[p].idMotion) + "ms. ";
    LabelS10->Hint = s;
    // Hints: 3-R�sistances du mob

    LabelS13->Hint = "";
    LabelS13->Hint = "Magic resistance: "+AnsiString(sMonsters[p].iMDef)
                 /*   +" Elem: " + AnsiString(mElem)
                    +" Type: " + AnsiString(mElemType)
                    +" Level: " + AnsiString(mElemLevel)   */
                    +"   ";
    switch (mElemType)
    {   case 0: LabelS13->Hint = LabelS13->Hint + "Protected vs Wand attack.";
            break;
        case 2: LabelS13->Hint = LabelS13->Hint + "Exposed to Lightning Strike.";
            if (mElemLevel == 0)
            {   LabelS13->Hint = LabelS13->Hint + "No special weakness.";
            }
            break;
        case 4: LabelS13->Hint = LabelS13->Hint + "Immune vs Lightning Strike.";
            break;
        default: LabelS13->Hint = LabelS13->Hint + "No special weakness.";
            break;
    }




//---------- SIMULATION JOUEUR SUR MOB -----------------------------------------
    // Calcul du % de toucher
    int pHit;
    if (RadioButton4->Checked) // A l'arc
    {   pHit = jHit - mFlee;
        // La distance r�duit le toucher a l'arc selon Mallard's Eye
        int malus_dist;
        if (CB2->ItemIndex == 2) malus_dist = 4 - int(CB2->Tag /75);
        else                     malus_dist = 4;
        if (malus_dist < 0) malus_dist = 0;
        pHit-= (malus_dist * (malus_dist + 1));
        if (CB2->ItemIndex == 2) // Mallard's Eye am�liore le toucher � l'arc
        {   pHit += CB2->Tag / 10;   // 20 for 200
        }
    }else                     // Au C�C le malus distance s'applique aussi!
    {    pHit = jHit - (mFlee-2);
        // Brawling am�liore le toucher
         if (CB2->ItemIndex == 5)  pHit += CB2->Tag/16;
    }
    if (pHit<5)   pHit = 5;       //-20:Range4, -30:Range5
    if (pHit>100) pHit = 100;     //-2: Range1, -6: Range2, -12:Range3


    // Calcul du � de critique (pour 1000)
    int pCri = jCri;
    pCri = pCri - sMonsters[p].iLUK * 2; // *3 quand le joueur est tapp�
    if (pCri<0) pCri=0;


    // Calcul des dommages
    int pDmg1, pDmg2, pDmgCri;  // Mini, Maxi, Critical
    float fDmg12;               // Moyenne
    int pDmgB;                  // pDmgB: Base Dmg due aux stats
    int pAtkMin, pAtkMax;       // pAtkMxx: Dmg dus � l'arme
    int iDmgWand1, iDmgWand2,iDmgLStk1, iDmgLStk2;            // Spell damage
    int iSpellDelayW, iChargesW, iSpellDelayLStk, iChargesLStk; // Spell Delay & charges

    if (RadioButton4->Checked)      // pDmgB: Archer
    {   pDmgB = iDex/10;
        pDmgB = pDmgB*pDmgB + iDex;
        pDmgB = pDmgB + iStr/5;
        pDmgB = pDmgB + iLuc/5;
        pAtkMin = iDex;
        pAtkMax = iDmg;
        if (pAtkMin>pAtkMax) pAtkMin = pAtkMax;
        pAtkMin = pAtkMin * pAtkMax/100;
        if (pAtkMin>pAtkMax) pAtkMax = pAtkMin;
        pDmg1   = pDmgB + pAtkMin;
        pDmg2   = pDmgB + pAtkMax + iArrowDmg;
        pDmgCri = pDmg2;
        // Calculs des dommages augment�s de loin
        /*for (int i = 0; i<= iMaxRange; i++)
        {   if (iDex<10)
            {   sRangeDmg[i].iAtk1 = pDmg1 + i*(iDex+5.5)/10;
                sRangeDmg[i].iAtk2 = pDmg2 + i*(iDex+5.5)/10;
                sRangeDmg[i].iAtkC = pDmg2 + i*(iDex+5.5)/10;
            }else
            {   sRangeDmg[i].iAtk1 = pDmg1 + i*(iDex+7.0)/10;
                sRangeDmg[i].iAtk2 = pDmg2 + i*(iDex+7.0)/10;
                sRangeDmg[i].iAtkC = pDmg2 + i*(iDex+7.0)/10;
            }
            sRangeDmg[i+1].iAtk1 = -1; // Marqueur de fin
         }    */
        for (int i = 0; i<= iMaxRange; i++)
        {   if (i <= 2)  // Formule du serveur
            {   sRangeDmg[i].iAtk1 = pDmg1;
                sRangeDmg[i].iAtk2 = pDmg2;
                sRangeDmg[i].iAtkC = pDmg2;
            }else
            {   sRangeDmg[i].iAtk1 = pDmg1 * (256+((80*i)/iMaxRange))/256;
                sRangeDmg[i].iAtk2 = pDmg2 * (256+((80*i)/iMaxRange))/256;
                sRangeDmg[i].iAtkC = pDmg2 * (256+((80*i)/iMaxRange))/256;
            }
            sRangeDmg[i+1].iAtk1 = -1; // Marqueur de fin
         }
         //damage =   damage * (256 +((80 * target_distance) / sd->attackrange)) >> 8;


        // Calculer la vraie moyenne sur l'ensemble des coups normaux...
        fDmg12 = fAverage(sRangeDmg[iMaxRange-1].iAtk1, sRangeDmg[iMaxRange-1].iAtk2, mDef, mDef2, iArrowDmg);
   }else
    {   pDmgB = iStr/10;            // pDmgB: C�C
        pDmgB = pDmgB*pDmgB + iStr;
        pDmgB = pDmgB + iDex/5;
        pDmgB = pDmgB + iLuc/5;
        pDmgCri = pDmgB + iDmg;     // Crit dmg
        pDmg2   = pDmgB + iDmg;     // Maxi
        pDmg1   = pDmgB + iDex;     // Mini
        if (pDmg1>pDmg2) pDmg1 = pDmg2;
        // Calculer la vraie moyenne sur l'ensemble des coups normaux...
        fDmg12 = fAverage(pDmg1, pDmg2, mDef, mDef2, 0);
    }

    // Prise en compte des r�ductions et absorbtion de dommages
    if (RadioButton4->Checked)  // Arc ou fronde
    {   for (int i = 0; i<= iMaxRange; i++)
        {   // D�gats r�duits par la def du Mob
            if (mDef>0) sRangeDmg[i].iAtk1 = (sRangeDmg[i].iAtk1*(100-mDef))/100;
            if (mDef>0) sRangeDmg[i].iAtk2 = (sRangeDmg[i].iAtk2*(100-mDef))/100;
            if(sRangeDmg[i].iAtk1<1) sRangeDmg[i].iAtk1=1;
            if(sRangeDmg[i].iAtk2<1) sRangeDmg[i].iAtk2=1;
            // D�gats absorb�s par la VIT du Mob.
            sRangeDmg[i].iAtk1 = sRangeDmg[i].iAtk1 - (mDef2*8)/10;
            sRangeDmg[i].iAtk2 = sRangeDmg[i].iAtk2 - (mDef2*8)/10;
            if(sRangeDmg[i].iAtk1<1) sRangeDmg[i].iAtk1=1;
            if(sRangeDmg[i].iAtk2<1) sRangeDmg[i].iAtk2=1;
            // Affichage
            if (i !=0)
            {   s = s + "@" + AnsiString(i)+": "
                    + AnsiString(sRangeDmg[i].iAtk1)+".."
                    + AnsiString(sRangeDmg[i].iAtk2)/*+" ("
                    + AnsiString(sRangeDmg[i].iAtkC)+")  "*/;
            }else s = "";
        }
        LabelS6->Hint = s;
        LabelS6->Caption = " @"+ AnsiString(iMaxRange)
                         + ": " + AnsiString(sRangeDmg[iMaxRange].iAtk1)
                         + ".." + AnsiString(sRangeDmg[iMaxRange].iAtk2)
                         + " (" + AnsiString(sRangeDmg[iMaxRange].iAtkC)+")";
        // Affichage des donn�es suppl�mentaire PJ sur Mob
        LabelS4->Caption = " Dmg: "+AnsiString(sRangeDmg[0].iAtk1)
                         + ".."    +AnsiString(sRangeDmg[0].iAtk2)
                         + " ("    +AnsiString(sRangeDmg[0].iAtkC)+")";
        LabelS4->Hint    = "At close range. Min damage: "   + AnsiString(sRangeDmg[0].iAtk1)
                         + "  Max damage: " + AnsiString(sRangeDmg[0].iAtk2)
                         + "  Critical damage: "+ AnsiString(sRangeDmg[0].iAtkC);
        Timer2->Tag = 0;
        Timer2->Enabled = true;
        // Utiliser les d�gats de MaxRange -1 pour la suite
        pDmg1 = sRangeDmg[iMaxRange-1].iAtk1;
        pDmg2 = sRangeDmg[iMaxRange-1].iAtk2;
        pDmgCri = sRangeDmg[iMaxRange-1].iAtkC;
    }else
    // C�C attack
    {   // D�gats r�duits par la def du Mob
        if (mDef>0) pDmg1 = (pDmg1*(100-mDef))/100;
        if (mDef>0) pDmg2 = (pDmg2*(100-mDef))/100;
        if(pDmg1<1)pDmg1=1;
        if(pDmg2<1)pDmg2=1;
        // D�gats absorb�s par la VIT du Mob.
        pDmg1 = pDmg1 - (mDef2*8)/10;
        pDmg2 = pDmg2 - (mDef2*8)/10;
        if(pDmg1<1)pDmg1=1;
        if(pDmg2<1)pDmg2=1;
        // Affichage des donn�es suppl�mentaire PJ sur Mob
        LabelS4->Caption = " Dmg: "+ AnsiString(pDmg1)
                         + ".."    + AnsiString(pDmg2)
                         + " ("    + AnsiString(pDmgCri)+")";
        LabelS4->Hint    = "Minimum damage: "   + AnsiString(pDmg1)
                         + "  Maximum damage: " + AnsiString(pDmg2)
                         + "  Critical damage: "+ AnsiString(pDmgCri);
    }

    //Remplacer par l'affichage des donn�es des sorts
    if ((RadioButton3->Checked)||(RadioButton5->Checked))  // Show Spell damage
    {   // int iDmgWand1, iDmgWand2,iDmgLStk1, iDmgLStk2;     // Spell damage
        // int iSpellDelayW, iChargesW, iSpellDelayLStk, iChargesLStk; // Spell Delay & charges
        //Wand attack
        iDmgWand1 = (iMAtk2 + 12)/3;           // +10 +School:0 +MagicLvl:2
        iDmgWand2 = iDmgWand1*2 - 1;
        iChargesW = 1+((iMAtk2 + 12)/10);      // +10 +School:0 +MagicLvl:2
        iSpellDelayW = (((200 - iAgi) * 1200) / 200);   //delay
        //mElemType, mElemLevel;
        if (mElemType == 0)
        {   iDmgWand1 = iDmgWand1 /3;
            iDmgWand1 = ((mElemLevel+4)*iDmgWand1) /4;
            iDmgWand2 = iDmgWand2 /3;
            iDmgWand2 = ((mElemLevel+4)*iDmgWand2) /4;
        }
        // Magic resistance: injure .@source, @target_id, (.@dmg * (100 - get(MDEF1, @target_id))) / 100;
        iDmgWand1 = (iDmgWand1*(100-mMDef))/100;
        iDmgWand2 = (iDmgWand2*(100-mMDef))/100;
        if (RadioButton5->Checked)   // D�gats augment�s et cout augment� avec baton
        {   iDmgWand1 *=2;
            iDmgWand2 *=2;
            iChargesW +=1;
        }
        // Lightning Strike attack
        iDmgLStk1 = iMAtk2 + 14;                 // 10+ School:2 +MagicLvl:2
        iDmgLStk2 = iDmgLStk1 + (iDmgLStk1/2) +1;         // No School:10 MagicLvl:12
        iSpellDelayLStk = (((200 - iAgi) * 3000) / 200);  //delay
        iChargesLStk = ((iMAtk2 + 14)/90)+1;
        // Bonus vs ELT_EARTH, Malus vs ELT_WIND
        if (mElemType == 2) // Bonus vs ELT_EARTH
        {   iDmgLStk1 = ((mElemLevel+4)*iDmgLStk1) /4;
            iDmgLStk2 = ((mElemLevel+4)*iDmgLStk2) /4;
        }
        if (mElemType == 4)  // Malus vs ELT_WIND
        {   iDmgLStk1 = iDmgLStk1/3;
            iDmgLStk2 = iDmgLStk2/3;
        }
        // Magic resistance: injure .@source, @target_id, (.@dmg * (100 - get(MDEF1, @target_id))) / 100;
        iDmgLStk1 = (iDmgLStk1*(100-mMDef))/100;
        iDmgLStk2 = (iDmgLStk2*(100-mMDef))/100;
        LabelS3->Caption = " Spell delay: " + AnsiString(iSpellDelayW) + "ms";
        // Write damage #confringo
        LabelS3a->Caption = " #confringo: "+AnsiString(iDmgWand1)+ ".."    +AnsiString(iDmgWand2);
        LabelS3a->Hint    = "Wand attack, #confingo: "  + AnsiString(iChargesW)+ " charges, "
                            + AnsiString(iSpellDelayW)+ "ms spell delay." + "|%%$$##W";
        LabelS3->Hint     = LabelS3a->Hint;
        LabelS3a->Tag     = iSpellDelayW;
        // Write damage #ingrav
        LabelS6->Caption = " Spell delay: " + AnsiString(iSpellDelayLStk) + "ms";
        LabelS4->Caption = " #ingrav: "+ AnsiString(iDmgLStk1)+ ".."    +AnsiString(iDmgLStk2);
        LabelS4->Hint    = "Lightning strike, #ingrav: "  + AnsiString(iChargesLStk)+ " charges, "
                            + AnsiString(iSpellDelayLStk)+ "ms spell delay." + "|%%$$##LS";
        LabelS6->Hint    = LabelS4->Hint;
        LabelS4->Tag     = iSpellDelayLStk;
    }

    // Calcul des probas de louper/toucher/criter sur 1000
    float fpMiss1, fpMiss2, fpCrit, fpHit;
    fpMiss1 = mFle2;
    fpCrit  = ((1000.0 - fpMiss1)*pCri/1000.0);
    fpHit   = ((1000.0 - (fpMiss1+fpCrit))*pHit/100.0);
    fpMiss2 = ((1000.0 - (fpMiss1+fpCrit))*(100-pHit)/100.0);

    // Affichage Hit miss crit dodge....
    s=FormatFloat("0.0",(fpCrit+fpHit)/10.0);
    LabelS1->Caption = " Hit proba:   "+FormatFloat("0.0",(fpHit+fpCrit)/10.0)+" %";
    LabelS1->Hint    = "Dodged: "+FormatFloat("0.0",fpMiss1/10.0)
                       +"%  Critical hit: "+FormatFloat("0.0",fpCrit/10.0)
                       +"%  Normal hit: "+FormatFloat("0.0",fpHit/10.0)
                       +"%  Miss: "+FormatFloat("0.0",fpMiss2/10.0)+"%";
    PanelPJ(this, fpMiss1/10, ((fpMiss1/10)+(pCri/10)), (100-(fpMiss2/10)));
    Panel8->Hint = LabelS1->Hint;

    // Calcul: DPS r�el sur le Mob en cours
    float fDmg12cm =  (fpCrit*pDmgCri + fpHit*fDmg12)/1000.0;  // D�gats moyen d'un coup (y compris rat�s et crits)

    // Afficher le DPS sauf si en mode sorts
    float fDPS = (500.0*fDmg12cm) / (iHitDelay*2);            // DPS
    if ((RadioButton3->Checked)||(RadioButton5->Checked))
    { }else
    {   LabelS3a->Caption = " Actual DPS on";
        LabelS3->Caption  = " current mob:  "+ FormatFloat("0.0",fDPS/1.0);
        LabelS3a->Hint    = "Shows the computed DPS on selected mob.";
        LabelS3->Hint     = "Shows the computed DPS on selected mob.";
    }
    // Affichage des dommages r�duits, absorb�s et critiques
    LabelS2->Caption = " Avg dmg:   "+FormatFloat("0.0",fDmg12cm);
    if (RadioButton4->Checked)  // Bow
    {   if (fpCrit==0.0)
        {   LabelS2->Hint ="Near max range, mitigated dmg: "+ AnsiString(pDmg1)+".."+AnsiString(pDmg2)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm);
        }else
        {   LabelS2->Hint ="Near max range, mitigated dmg: "+ AnsiString(pDmg1)+".."+AnsiString(pDmg2)
                    +"  Critical dmg: "+ AnsiString(pDmgCri)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm);
        }
    }else  // C�C
    {   if (fpCrit==0.0)
        {   LabelS2->Hint ="Mitigated dmg: "+ AnsiString(pDmg1)+".."+AnsiString(pDmg2)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm);
        }else
        {   LabelS2->Hint ="Mitigated dmg: "+ AnsiString(pDmg1)+".."+AnsiString(pDmg2)
                    +"  Critical dmg: "+ AnsiString(pDmgCri)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm);
        }
    }

    // Calcul: Time to Kill
    int iTime = ceil(mHP/fDPS); // Calculer le temps pour tuer en 10eme de seconde
    if (iTime>3600)                                    // 1 heure
    {   LabelS5->Caption = " Kill time:     > 1hour";
    }else if (iTime>299)                               // 5 minutes
    {   s=FormatFloat("0.",iTime/60.0);
        LabelS5->Caption = " Kill time:     "+ AnsiString(s)+"min";
    }else if ((pDmg1>=mHP)&&(pHit>80))                   // 1 shot
    {   LabelS5->Caption = " Mob is one-shoot !";
    }else
    {   LabelS5->Caption = " Kill time:     "+ AnsiString(iTime)+"\"";
    }


//---------- SIMULATION MOB SUR JOUEUR -----------------------------------------
    // Calcul du % d'�tre touch� par le mob
    pHit =  mHit - (jFlee+2);       //-2: Range1 -6: Range2, -12:Range3
    if (pHit<5)   pHit = 5;         //-20:Range4, -30:Range5
    if (pHit>100) pHit = 100;

    // Calcul du � de critique du Mob (pour 1000)
    pCri = mCri - iLuc * 3; // *3 quand le joueur est tapp�
    if (pCri<0) pCri=0;

   // Calcul des dommages du Mob
    pDmgB = mStr/10;          // pDmgB: Bonus du a la force du Mob
    pDmgB = pDmgB*pDmgB + mStr;
    pDmgCri = pDmgB + mAtk2;     // Crit dmg
    pDmg1   = pDmgB + mAtk1 ;    // Mini
    pDmg2   = pDmgB + mAtk2;     // Maxi
    if (pDmg1>pDmg2) pDmg1 = pDmg2;
    int pDmg13 = pDmgB + mAtk1 ;    // Mini
    int pDmg23 = pDmgB + mAtk2 ;    // Maxi
    if (pDmg13>pDmg23) pDmg13 = pDmg23;
    int pDmg15 = pDmgB + mAtk1 ;    // Mini
    int pDmg25 = pDmgB + mAtk2 ;    // Maxi
    if (pDmg15>pDmg25) pDmg15 = pDmg25;

    // Calculer la vraie moyenne sur l'ensemble des coups...
    fDmg12 = fAverage(pDmg1, pDmg2, jDef, jDef2, 0);            // Pas de fl�ches pour les mobs
    float fDmg123 = fAverage(pDmg13, pDmg23, jDef3, jDef23, 0);
    float fDmg125 = fAverage(pDmg15, pDmg25, jDef5, jDef25, 0);

    // D�gats r�duits par la def du joueur
    if (jDef>0) pDmg1 = (pDmg1*(100-jDef))/100;
    if (jDef>0) pDmg2 = (pDmg2*(100-jDef))/100;
    // Et DEF r�duite par le nombre de mobs
    if (jDef3>0) pDmg13 =  pDmg13*(100-(jDef3))/100;
    if (jDef3>0) pDmg23 =  pDmg23*(100-(jDef3))/100;
    if (jDef5>0) pDmg15 =  pDmg15*(100-(jDef5))/100;
    if (jDef5>0) pDmg25 =  pDmg25*(100-(jDef5))/100;
    if(pDmg1<1)pDmg1=1;
    if(pDmg2<1)pDmg2=1;
    if(pDmg13<1)pDmg13=1;
    if(pDmg23<1)pDmg23=1;
    if(pDmg15<1)pDmg15=1;
    if(pDmg25<1)pDmg25=1;

    // D�gats absorb�s par la VIT normale du joueur.
    pDmg1 = pDmg1 - (jDef2*8)/10;
    pDmg2 = pDmg2 - (jDef2*8)/10;
    if(pDmg1<1)pDmg1=1;
    if(pDmg2<1)pDmg2=1;
    // D�gats absorb�s par la VIT r�duite  du joueur.
    pDmg13 = pDmg13 - (jDef23*8)/10;
    pDmg23 = pDmg23 - (jDef23*8)/10;
    if(pDmg13<1)pDmg13=1;
    if(pDmg23<1)pDmg23=1;
    pDmg15 = pDmg15 - (jDef25*8)/10;
    pDmg25 = pDmg25 - (jDef25*8)/10;
    if(pDmg15<1)pDmg15=1;
    if(pDmg25<1)pDmg25=1;


    // Calcul des probas de louper/toucher/criter sur 1000
    // float fpMiss1, fpMiss2, fpCrit, fpHit;
    fpMiss1 = jFle2;
    fpCrit  = ((1000.0 - fpMiss1)*pCri/1000.0);
    fpHit   = ((1000.0 - (fpMiss1+fpCrit))*pHit/100.0);
    fpMiss2 = ((1000.0 - (fpMiss1+fpCrit))*(100-pHit)/100.0);

    // Affichage Dodge Crit Hit Miss ....
    LabelS7->Caption = " Wnd proba: "+FormatFloat("0.0",(fpHit+fpCrit)/10.0)+" %";
    LabelS7->Hint    = "Dodged: "+FormatFloat("0.0",fpMiss1/10.0)
                       +"%  Critical hit: "+FormatFloat("0.0",fpCrit/10.0)
                       +"%  Normal hit: "+FormatFloat("0.0",fpHit/10.0)
                       +"%  Miss: "+FormatFloat("0.0",fpMiss2/10.0)+"%";
    PanelMob(this, fpMiss1/10, (fpMiss1/10)+(pCri/10), (100-(fpMiss2/10)));
    Panel4->Hint = LabelS7->Hint;

    // Calcul: DPS r�el sur le Mob en cours
    fDmg12cm =  (fpCrit*pDmgCri + fpHit*fDmg12)/1000.0;   // D�gats moyen d'un coup (y compris rat�s et crits)
    fDPS = (1000.0*fDmg12cm) / sMonsters[p].iAdelay ;     // DPS
    // Afficher le DPS
    LabelS8->Caption = " Avg wnd:    "+FormatFloat("0.0",fDmg12cm);
    if (fpCrit==0.0)
    {   LabelS8->Hint = "Mitigated dmg: "+ AnsiString(pDmg1)+".."+AnsiString(pDmg2)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm)
                    +"  Mob's DPS: "+ FormatFloat("0.0",fDPS);
    }else
    {   LabelS8->Hint = "Mitigated dmg: "+ AnsiString(pDmg1)+".."+AnsiString(pDmg2)
                    +"  Critical dmg: "+ AnsiString(pDmgCri)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm)
                    +"  Mob's DPS: "+ FormatFloat("0.0",fDPS);
    }

    // Calculer temps de survie et DPS du Mob
    // Calculer HP et regen du Player
    int iHP = iLev*5 + 35;
    iHP = iHP*(iVit+100)/100;
    int iHPr = 1 + iVit/5 + iHP/200;

    // Regen >= DPS
    fDPS = fDPS - (iHPr/6);
    if (fDPS <= 1)
    {   LabelS9->Caption = " No danger...";
        LabelS9->Hint ="A non-mutated mob shouldn't be able to kill you.";
    }else // Calcul: Time to Kill
    {   iTime = floor(iHP/(fDPS)); // Calculer le temps pour tuer en 10eme de seconde
        if ((pDmg1>=iHP)&&(pHit>90))                            // 1 shot
        {   LabelS9->Caption = " Player one-shoot !";
            LabelS9->Hint ="Mob's minimum damage will kill you!";
        }else if (pDmgCri >= iHP)                               // Possible 1 shot
        {   LabelS9->Caption = " Possible one-shoot !";
            LabelS9->Hint ="Mob's maximum damage could kill you!";
        }else
        {   LabelS9->Caption = " Survival:     "+ AnsiString(iTime)+"\"";
            LabelS9->Hint ="Average survival time when attacked by a non-mutated mob.";
    }   }

    // Affichage sur 3 ou 5 mobs: Hit
    int pHit3 = mHit - ((jFlee*0.90)+2); //-2: Range1 -6: Range2, -12:Range3
    if (pHit3<5)   pHit3 = 5;            //-20:Range4, -30:Range5
    if (pHit3>100) pHit3 = 100;
    int fpMiss23 = ((1000.0 - (fpMiss1+fpCrit))*(100-pHit3)/100.0);
    float fpHit3   = ((1000.0 - (fpMiss1+fpCrit))*pHit3/100.0);
    LabelS37->Caption = " Wnd proba: "+FormatFloat("0.0",(fpHit3+fpCrit)/10.0)+" %";
    LabelS37->Hint    = "Dodged: "+FormatFloat("0.0",fpMiss1/10.0)
                       +"%  Critical hit: "+FormatFloat("0.0",fpCrit/10.0)
                       +"%  Normal hit: "+FormatFloat("0.0",fpHit3/10.0)
                       +"%  Miss: "+FormatFloat("0.0",fpMiss23/10.0)+"%";
    PanelMob3(this, fpMiss1/10, (fpMiss1/10)+(pCri/10), (100-(fpMiss23/10)));
    Panel34->Hint = LabelS37->Hint;

    int pHit5 = mHit - ((jFlee*0.70)+2); //-2: Range1 -6: Range2, -12:Range3
    if (pHit5<5)   pHit5 = 5;            //-20:Range4, -30:Range5
    if (pHit5>100) pHit5 = 100;
    int fpMiss25 = ((1000.0 - (fpMiss1+fpCrit))*(100-pHit5)/100.0);
    float fpHit5   = ((1000.0 - (fpMiss1+fpCrit))*pHit5/100.0);
    LabelS57->Caption = " Wnd proba: "+FormatFloat("0.0",(fpHit5+fpCrit)/10.0)+" %";
    LabelS57->Hint    = "Dodged: "+FormatFloat("0.0",fpMiss1/10.0)
                       +"%  Critical hit: "+FormatFloat("0.0",fpCrit/10.0)
                       +"%  Normal hit: "+FormatFloat("0.0",fpHit5/10.0)
                       +"%  Miss: "+FormatFloat("0.0",fpMiss25/10.0)+"%";
    PanelMob5(this, fpMiss1/10, (fpMiss1/10)+(pCri/10), (100-(fpMiss25/10)));
    Panel54->Hint = LabelS57->Hint;

    // Affichage sur 3 mobs: Wounds
    // Calcul: DPS r�el par le Mob en cours
    fDmg12cm =  (fpCrit*pDmgCri + fpHit3*fDmg123)/1000.0;  // D�gats moyen d'un coup (y compris rat�s et crits)
    fDPS = (1000.0*fDmg12cm) / sMonsters[p].iAdelay ;     // DPS
    // Afficher le DPS
    LabelS38->Caption = " Avg wnd:    "+FormatFloat("0.0",fDmg12cm);
    if (fpCrit==0.0)
    {    LabelS38->Hint = "Mitigated dmg: "+ AnsiString(pDmg13)+".."+AnsiString(pDmg23)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm)
                    +"  Mobs DPS:  "+ FormatFloat("0.0",fDPS)+"x3 = "+FormatFloat("0.0",fDPS*3);
    }else
    {    LabelS38->Hint = "Mitigated dmg: "+ AnsiString(pDmg13)+".."+AnsiString(pDmg23)
                    +"  Critical dmg: "+ AnsiString(pDmgCri)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm)
                    +"  Mobs DPS:  "+ FormatFloat("0.0",fDPS)+"x3 = "+FormatFloat("0.0",fDPS*3);
    }
    fDPS = (3*fDPS) - (iHPr/6);
    if (fDPS <= 1)
    {   LabelS39->Caption = " No danger...";
        LabelS39->Hint ="Even 3 non-mutated mobs shouldn't be able to kill you.";
    }else // Calcul: Time to Kill
    {   iTime = floor(iHP/(fDPS)); // Calculer le temps pour tuer en 10eme de seconde
        if (iTime <=1)                            // 1 shot
        {   LabelS39->Caption = " Less than 1\" !";
            LabelS39->Hint ="Together, 3 mobs will kill you very fast!";
        }else
        {   s=FormatFloat("0",iTime);
            LabelS39->Caption = " Survival:     "+ AnsiString(s)+"\"";
            LabelS39->Hint ="Average survival time when attacked by 3 non-mutated mobs.";
    }   }

    // Affichage sur 5 mobs: Wounds
    // Calcul: DPS r�el par le Mob en cours
    fDmg12cm =  (fpCrit*pDmgCri + fpHit5*fDmg125)/1000.0;  // D�gats moyen d'un coup (y compris rat�s et crits)
    fDPS = (1000.0*fDmg12cm) / sMonsters[p].iAdelay ;     // DPS
    // Afficher le DPS
    LabelS58->Caption = " Avg wnd:    "+FormatFloat("0.0",fDmg12cm);

    if (fpCrit==0.0)
    {   LabelS58->Hint = "Mitigated dmg: "+ AnsiString(pDmg15)+".."+AnsiString(pDmg25)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm)
                    +"  Mobs DPS:  "+ FormatFloat("0.0",fDPS)+"x5 = "+FormatFloat("0.0",fDPS*5);
    }else
    {   LabelS58->Hint = "Mitigated dmg: "+ AnsiString(pDmg15)+".."+AnsiString(pDmg25)
                    +"  Critical dmg: "+ AnsiString(pDmgCri)
                    +"  Average: "+ FormatFloat("0.0",fDmg12cm)
                    +"  Mobs DPS:  "+ FormatFloat("0.0",fDPS)+"x5 = "+FormatFloat("0.0",fDPS*5);
    }

    // Regen >= DPS
    fDPS = (5*fDPS) - (iHPr/6);
    if (fDPS <= 1)
    {   LabelS59->Caption = " No danger...";
        LabelS59->Hint ="Even 5 non-mutated mobs shouldn't be able to kill you.";
    }else // Calcul: Time to Kill
    {   iTime = floor(iHP/(fDPS)); // Calculer le temps pour tuer en 10eme de seconde
        if (iTime <=1)                            // 1 shot
        {   LabelS59->Caption = " Less than 1\" !";
            LabelS59->Hint ="Together, 5 mobs will kill you very fast!";
        }else
        {   s=FormatFloat("0",iTime);
            LabelS59->Caption = " Survival:     "+ AnsiString(s)+"\"";
            LabelS59->Hint ="Average survival time when attacked by 5 non-mutated mobs.";
    }   }

    EstimerStun(this);
}
//---------------------------------------------------------------------------
// Load a Mob from a config file
//---------------------------------------------------------------------------
void __fastcall TForm1::LoadMob(TObject *Sender)
{   //Charger le fichier contenant les donn�es
    TStringList * ListeMobs = new TStringList;
    TStringList * sLigne    = new TStringList;
try{
    AnsiString Path;
    OpenDialog1->InitialDir = ExtractFilePath(Application->ExeName)+"\\";
    if (OpenDialog1->Execute())
    {   // Un fichier a �t� ouvert.
        Path = OpenDialog1->FileName;
        ListeMobs->LoadFromFile(Path);
        ListeMobs->Sorted = false;
        CB1->Clear();
        AnsiString l;
        int iIndex = 0;
        for (int i = 0; i < ListeMobs->Count; i++)
        {   sLigne->CommaText = ListeMobs->Strings[i];
            l = sLigne->Strings[0];
            if (l.ToIntDef(-1) == -1) continue;
            sMonsters[iIndex].sName = sLigne->Strings[1];
            sMonsters[iIndex].iLV   = sLigne->Strings[3].ToIntDef(1);
            sMonsters[iIndex].iHP   = sLigne->Strings[4].ToIntDef(99);
            sMonsters[iIndex].iRange = sLigne->Strings[8].ToIntDef(50);
            sMonsters[iIndex].iATK1 = sLigne->Strings[9].ToIntDef(1);
            sMonsters[iIndex].iATK2 = sLigne->Strings[10].ToIntDef(2);
            sMonsters[iIndex].iDEF  = sLigne->Strings[11].ToIntDef(0);
            sMonsters[iIndex].iMDef = sLigne->Strings[12].ToIntDef(0);
            sMonsters[iIndex].iSTR  = sLigne->Strings[13].ToIntDef(1);
            sMonsters[iIndex].iAGI  = sLigne->Strings[14].ToIntDef(1);
            sMonsters[iIndex].iVIT  = sLigne->Strings[15].ToIntDef(1);
            sMonsters[iIndex].iINT  = sLigne->Strings[16].ToIntDef(1);
            sMonsters[iIndex].iDEX  = sLigne->Strings[17].ToIntDef(1);
            sMonsters[iIndex].iLUK  = sLigne->Strings[18].ToIntDef(1);
            sMonsters[iIndex].iElem = sLigne->Strings[23].ToIntDef(20);
            sMonsters[iIndex].iMode = sLigne->Strings[24].ToIntDef(129);
            sMonsters[iIndex].iSpeed = sLigne->Strings[25].ToIntDef(999);
            sMonsters[iIndex].iAdelay = sLigne->Strings[26].ToIntDef(2999);
            sMonsters[iIndex].idMotion = sLigne->Strings[28].ToIntDef(480);
            CB1->Items->Add(sMonsters[iIndex].sName);
            iIndex++;
        }
        CB1->ItemIndex = 0;
        CB1Change(this);

    }else  // Pas de fichier ouvert
    {   CB1->ItemIndex = 0;
    }
}
__finally{
    delete ListeMobs;
    delete sLigne;
}
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// ContextMenu pour charger un fichier de d�finition de Mobs
//---------------------------------------------------------------------------
void __fastcall TForm1::Lod1Click(TObject *Sender)
{    LoadMob(this);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Default1Click(TObject *Sender)
{   CB1->Clear();
    LoadDefautMobs(this);
    CB1->ItemIndex = 0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// ContextMenu changement d'arme
//---------------------------------------------------------------------------
void __fastcall TForm1::PopupArme(TObject *Sender)
{   // Knife1->Checked = false;
    if( dynamic_cast<TMenuItem *>(Sender) !=0)
    {   iDmg = dynamic_cast <TMenuItem *>(Sender) ->Tag;
        // ShowMessage(AnsiString(iDmg));
        Button1->Caption = dynamic_cast <TMenuItem *>(Sender) ->Caption;
    };
    // Cacher les bonus caract�ristiques des armes si elles y sont d�j�.
    if (LabelIG->Tag == 1)
    {   UpDown16->Position -= 1;
        LabelIG->Tag = 0;
        LabelIG->Hide();
    }
    if (LabelRK->Tag == 3)
    {   UpDown13->Position -=3;
        LabelRK->Tag = 0;
        LabelRK->Hide();
    }
    Timer1->Enabled = true;
    int iOld = iDmgView;
    if (RadioButton4->Checked)
    {   LabeledEditO2->Color = clHighlight;
        iDmgView = 11; // Ranged Weapon speed
        if (LabelFA->Tag == 3)  // Virer Forest Armor
        {   UpDown15->Position -= 3;
            LabelFA->Tag = 0;
            LabelFA->Hide();
        }
        if (dynamic_cast <TMenuItem *>(Sender) == MenuItem12)
        {   iArrowDmg = 3;
            //iMaxRange = 3;
            Button1->Tag = 3;
            CheckMaxRange(this, 3);
        }else
        {   iArrowDmg = 20;
            //iMaxRange = 5;
            Button1->Tag = 5;
            CheckMaxRange(this, 5);
        }
    }else
    {   LabeledEditO1->Color = clHighlight;
        iArrowDmg = 0;
        iMaxRange = 0;
        Button1->Tag = 0;
        if ((dynamic_cast <TMenuItem *>(Sender) == Scyte1)
            ||(dynamic_cast <TMenuItem *>(Sender) == Setzer1))
        {     iDmgView = 3;
        }else iDmgView = 1;
        // Tests des armes � bonus
        if (dynamic_cast <TMenuItem *>(Sender) == Icegladius1)
        {   // plus 1 bonus en Luck
            UpDown16->Position += 1;
            LabelIG->Tag = 1;
            LabelIG->Show();
        }else if (dynamic_cast <TMenuItem *>(Sender) == RockKnife1)
        {   // plus 3 bonus en Vita
            UpDown13->Position += 3;
            LabelRK->Tag = 3;
            LabelRK->Show();
        }
    }
    if (iDmgView != iOld) LabeledEditO5->Color = clHighlight;

    this->Bonus_OnChange(this);
    
}
//---------------------------------------------------------------------------
// Allouer le bon popup au bouton de choix d'arme
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{   if  ((RadioButton2->Checked)||(RadioButton6->Checked)||(RadioButton7->Checked))
    {    PopupMenuCaC->Popup(Mouse->CursorPos.x, Mouse->CursorPos.y);
    }else if (RadioButton4->Checked) // Range
    {    PopupMenuRange->Popup(Mouse->CursorPos.x, Mouse->CursorPos.y);
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PanelMob(TObject *Sender, int iV1, int iV2, int iV3)
{ // Retailler les Panels pour attaque de Mob
    int iX1, iX2, iX3, iX4;
    iX4 = Panel4->Width;
    if (iX4 ==0) return;
    iX1 = floor( iX4 * (iV1 * 0.01) );
    iX2 = floor( iX4 * (iV2 * 0.01) );
    iX3 = floor( iX4 * (iV3 * 0.01) );
    Panel1->Width = iX1;
    Panel2->Width = iX2;
    Panel3->Width = iX3;
    Panel1->Left = 0; //Panel4-> Left;
    Panel2->Left = 0; //Panel4-> Left;
    Panel3->Left = 0; //Panel4-> Left;
    Panel1->Color = clSilver;   // Dodges
    Panel2->Color = clRed;      // Crits
    Panel3->Color = clYellow;   // Hits
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PanelMob3(TObject *Sender, int iV1, int iV2, int iV3)
{ // Retailler les Panels pour attaque de Mob
    int iX1, iX2, iX3, iX4;
    iX4 = Panel34->Width;
    if (iX4 ==0) return;
    iX1 = floor( iX4 * (iV1 * 0.01) );
    iX2 = floor( iX4 * (iV2 * 0.01) );
    iX3 = floor( iX4 * (iV3 * 0.01) );
    Panel31->Width = iX1;
    Panel32->Width = iX2;
    Panel33->Width = iX3;
    Panel31->Left = 0; //Panel4-> Left;
    Panel32->Left = 0; //Panel4-> Left;
    Panel33->Left = 0; //Panel4-> Left;
    Panel31->Color = clSilver;   // Dodges
    Panel32->Color = clRed;      // Crits
    Panel33->Color = clYellow;   // Hits
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PanelMob5(TObject *Sender, int iV1, int iV2, int iV3)
{ // Retailler les Panels pour attaque de Mob
    int iX1, iX2, iX3, iX4;
    iX4 = Panel54->Width;
    if (iX4 ==0) return;
    iX1 = floor( iX4 * (iV1 * 0.01) );
    iX2 = floor( iX4 * (iV2 * 0.01) );
    iX3 = floor( iX4 * (iV3 * 0.01) );
    Panel51->Width = iX1;
    Panel52->Width = iX2;
    Panel53->Width = iX3;
    Panel51->Left = 0; //Panel4-> Left;
    Panel52->Left = 0; //Panel4-> Left;
    Panel53->Left = 0; //Panel4-> Left;
    Panel51->Color = clSilver;   // Dodges
    Panel52->Color = clRed;      // Crits
    Panel53->Color = clYellow;   // Hits
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PanelPJ(TObject *Sender, int iV1, int iV2, int iV3)
{ // Retailler les Panels pour attaque de Mob
    int iX1, iX2, iX3, iX4;
    iX4 = Panel8->Width;
    if (iX4 ==0) return;
    iX1 = floor( iX4 * (iV1 * 0.01) );
    iX2 = floor( iX4 * (iV2 * 0.01) );
    iX3 = floor( iX4 * (iV3 * 0.01) );
    Panel5->Width = iX1;
    Panel6->Width = iX2;
    Panel7->Width = iX3;
    Panel5->Left = 0; //Panel8-> Left;
    Panel6->Left = 0; //Panel8-> Left;
    Panel7->Left = 0; //Panel8-> Left;
    Panel5->Color = clSkyBlue;     // Dodges
    Panel6->Color = clPurple;      // Crits
    Panel7->Color = clLime;        // Hits
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PanelStun(TObject *Sender, int iV1, int iV2, int iV3, int iV4, int iVK1, int iVK2, int iPos)
{   // Retailler les images pour visualiser les chance de stun
    /*  Pour m�moire:
     iV1  = idMotion;
     iV2  = idMotion + iSpeed/4;
     iV3  = idMotion + (iSpeed*1.4)/4;
     iV4  = idMotion + (iSpeed*1.4)/4 +100;
    */
    // entre 500 et 1200 position: 8 et 361
    int iX1, iX2, iX3, iX4;
    // Le Wisp est tellement diff�rent qu'il a une image pr� cr�e pour lui tout seul.
    bool bWisp;
    if (iV1 == 1500) bWisp = true;
    else bWisp = false;
    if (bWisp)
    {   ImageWisp->Left = 0;
        ImageWisp->Width = 350;
        ImageWisp->Show();
        ImageWisp->BringToFront();
        LabelStun0->Caption = "595";
        LabelStun0->Left = 41;
        LabelStun0->Show();
        LabelStun1->Caption = "633";
        LabelStun1->Left = 65;
        LabelStun2->Caption = "750";
        LabelStun2->Left = 122;
        LabelStun3->Caption = "845";
        LabelStun3->Left = 170;
        LabelStun4->Caption = "983";
        LabelStun4->Left = 238;
        //Label7->Caption=("Tag : Wisp "+ AnsiString(iV1) +" "+AnsiString(iV2));
        goto Fin_Afficher_Marqueurs;
    }else ImageWisp->Hide();
    
    iX1 = floor( (iV1-500)/2 );
    if (iX1<5) iX1 = 5; // Toujours montrer un peu de Mauve
    iX2 = floor( (iV2-500)/2 );
    if (iX2<5) iX2 = 5; // Toujours montrer un peu de Bleu
    iX3 = floor( (iV3-500)/2 );
    if (iX3<5) iX3 = 5; // Toujours montrer un peu de Vert
    iX4 = floor( (iV4-500)/2 );
    if (iX4<5) iX4 = 5; // Toujours montrer un peu de jaune

    ImageS1->Top =1;
    ImageS2->Top =1;
    ImageS3->Top =1;
    ImageS4->Top =1;  
    LabelStun0->Hide();
    if((iV1 == -98)||(iV1 == -99))        // Plantes et Mobs statiques
    {   // Cas particulier, mob statique
        ImageS1->Hide();
        ImageMJB->Hide();
        ImageS2->Hide();
        ImageBV->Hide();
        ImageS3->Hide();
        ImageVJ->Hide();
        ImageS4->Hide();
        ImageJN->Hide();
        ImageS5->Hide();
        if (iV1 == -99)
        {   PanelS->Caption = " Static entity.";
            PanelS->Color = clBlue;
        }else
        {   PanelS->Caption = " Mob's range too long for stunning.";
            PanelS->Color = clBlack;  
        }
        PanelS->Font->Color = clYellow;
        LabelStun1->Caption = "";
        LabelStun2->Caption = "";
        LabelStun3->Caption = "";
        LabelStun4->Caption = "";
    }else if(iV4 <500)       // Le PJ le + rapide ne peut pas stunner
    {   // Cas extr�me, ==> Pas de stun calcul�
        ImageS1->Hide();
        ImageMJB->Hide();
        ImageS2->Hide();
        ImageBV->Hide();
        ImageS3->Hide();
        ImageVJ->Hide();
        ImageS4->Hide();
        ImageJN->Hide();
        ImageS5->Hide();
        PanelS->Caption = " Stun not possible.";
        PanelS->Font->Color = clWhite;
        PanelS->Color = clBlack;
        LabelStun1->Caption = "";
        LabelStun2->Caption = "";
        LabelStun3->Caption = "";
        LabelStun4->Caption = "";
    }else if (iV3 <500) // Seulement la barre jaune apparait
    {   ImageS1->Hide();
        ImageMJB->Hide();
        ImageS2->Hide();
        ImageBV->Hide();
        ImageS3->Hide();
        ImageVJ->Hide();
        ImageS4->Left = 1;  //PanelS-> Left;
        ImageS4->Width = iX4 - 4;
        ImageS4->Show();
        ImageJN->Left = ImageS4->Left + ImageS4->Width;
        ImageJN->Show();
        ImageJN->BringToFront();
        ImageS5->Left = iX4+3; //PanelS-> Left;
        ImageS5->Width = (PanelS->Width - ImageS5->Left)-1;
        ImageS5->Show();
        LabelStun1->Caption = "";
        LabelStun2->Caption = "";
        LabelStun3->Caption = "";
        LabelStun4->Caption = iV4;
        LabelStun4->Left = 8 + iX4 - (LabelStun4->Width/2);
        if (LabelStun4->Left < 3)
        {   LabelStun4->Left = 3;
        }

    }else if (iV2 <500) // Les barres verte et jaune apparaissent
    {   ImageS1->Hide();
        ImageMJB->Hide();
        ImageS2->Hide();
        ImageBV->Hide();
        ImageS3->Left = 1; //PanelS-> Left;
        ImageS3->Width = iX3 - 4;
        ImageS3->Show();
        ImageVJ->Left = ImageS3->Left + ImageS3->Width;
        ImageVJ->Show();
        ImageVJ->BringToFront();
        ImageS4->Left = ImageVJ->Left + ImageVJ->Width; //PanelS-> Left;
        ImageS4->Width = (iX4 - ImageS4->Left)-3;
        ImageS4->Show();
        ImageJN->Left = ImageS4->Left + ImageS4->Width;
        ImageJN->Show();
        ImageJN->BringToFront();
        ImageS5->Left = iX4+3; //PanelS-> Left;
        ImageS5->Width = (PanelS->Width - ImageS5->Left)-1;
        ImageS5->Show();
        LabelStun1->Caption = "";
        LabelStun2->Caption = "";
        LabelStun3->Caption = iV3;
        LabelStun3->Left = 8 + iX3 - (LabelStun3->Width/2);
        if (LabelStun3->Left < 3)
        {   LabelStun3->Left = 3;
        }
        LabelStun4->Caption = iV4;
        LabelStun4->Left = 8 + iX4 - (LabelStun4->Width/2);
        if (LabelStun4->Left < (LabelStun3->Left + LabelStun3->Width + 4))
        {   LabelStun4->Left = LabelStun3->Left + LabelStun3->Width + 4;
        }
    }else if (iV1 <500) // Les barres bleues, verte et jaune apparaissent
    {   ImageS1->Hide();
        ImageMJB->Hide();
        ImageS2->Left = 1; //PanelS-> Left;
        ImageS2->Width = iX2 - 4;
        ImageS2->Show();
        ImageBV->Left = ImageS2->Left + ImageS2->Width;
        ImageBV->Show();
        ImageBV->BringToFront();
        ImageS3->Left = ImageBV->Left + ImageBV->Width; //PanelS-> Left;
        ImageS3->Width = (iX3 - ImageS3->Left)-3;
        ImageS3->Show();
        ImageVJ->Left = ImageS3->Left + ImageS3->Width;
        ImageVJ->Show();
        ImageVJ->BringToFront();
        ImageS4->Left = ImageVJ->Left + ImageVJ->Width; //PanelS-> Left;
        ImageS4->Width = (iX4 - ImageS4->Left)-3;
        ImageS4->Show();
        ImageJN->Left = ImageS4->Left + ImageS4->Width;
        ImageJN->Show();
        ImageJN->BringToFront();
        ImageS5->Left = iX4+3; //PanelS-> Left;
        ImageS5->Width = (PanelS->Width - ImageS5->Left)-1;
        ImageS5->Show();
        LabelStun1->Caption = "";
        LabelStun2->Caption = iV2;
        LabelStun2->Left = 8 + iX2 - (LabelStun2->Width/2);
        if (LabelStun2->Left < 3)
        {   LabelStun2->Left = 3;
        }
        LabelStun3->Caption = iV3;
        LabelStun3->Left = 8 + iX3 - (LabelStun3->Width/2);
        if (LabelStun3->Left < (LabelStun2->Left + LabelStun2->Width + 4))
        {   LabelStun3->Left = LabelStun2->Left + LabelStun2->Width + 4;
        }
        LabelStun4->Caption = iV4;
        LabelStun4->Left = 8 + iX4 - (LabelStun4->Width/2);
        if (LabelStun4->Left < (LabelStun3->Left + LabelStun3->Width + 4))
        {   LabelStun4->Left = LabelStun3->Left + LabelStun3->Width + 4;
        }

    }else               // Toutes les barres apparaissent
    {   ImageS1->Left = 1;
        ImageS1->Width = iX1 -4;
        ImageS1->Show();
        ImageMJB->Left = ImageS1->Left + ImageS1->Width;
        ImageMJB->Show();
        ImageMJB->BringToFront();
        ImageS2->Left = ImageMJB->Left + ImageMJB->Width; //PanelS-> Left;
        ImageS2->Width = (iX2 - ImageS2->Left)-3;
        ImageS2->Show();
        ImageBV->Left = ImageS2->Left + ImageS2->Width;
        ImageBV->Show();
        ImageBV->BringToFront();
        ImageS3->Left = ImageBV->Left + ImageBV->Width; //PanelS-> Left;
        ImageS3->Width = (iX3 - ImageS3->Left)-3;
        ImageS3->Show();
        ImageVJ->Left = ImageS3->Left + ImageS3->Width;
        ImageVJ->Show();
        ImageVJ->BringToFront();
        ImageS4->Left = ImageVJ->Left + ImageVJ->Width; //PanelS-> Left;
        ImageS4->Width = (iX4 - ImageS4->Left)-3;
        ImageS4->Show();
        ImageJN->Left = ImageS4->Left + ImageS4->Width;
        ImageJN->Show();
        ImageJN->BringToFront();
        ImageS5->Left = iX4+3; //PanelS-> Left;
        ImageS5->Width = (PanelS->Width - ImageS5->Left)-1;
        ImageS5->Show();
        LabelStun1->Caption = iV1;
        LabelStun1->Left = 8 + iX1 - (LabelStun1->Width/2);
        LabelStun2->Left = 8 + iX2 - (LabelStun2->Width/2);
        if (LabelStun1->Left < 3)
        {   LabelStun1->Left = 3;
        }
        LabelStun2->Caption = iV2;
        LabelStun2->Left = 8 + iX2 - (LabelStun2->Width/2);
        if (LabelStun2->Left < (LabelStun1->Left + LabelStun1->Width + 4))
        {   LabelStun2->Left = LabelStun1->Left + LabelStun1->Width + 4;
        }
        LabelStun3->Caption = iV3;
        LabelStun3->Left = 8 + iX3 - (LabelStun3->Width/2);
        if (LabelStun3->Left < (LabelStun2->Left + LabelStun2->Width + 4))
        {   LabelStun3->Left = LabelStun2->Left + LabelStun2->Width + 4;
        }
        LabelStun4->Caption = iV4;
        LabelStun4->Left = 8 + iX4 - (LabelStun4->Width/2);
        if (LabelStun4->Left < (LabelStun3->Left + LabelStun3->Width + 4))
        {   LabelStun4->Left = LabelStun3->Left + LabelStun3->Width + 4;
        }   
    }
    // Traiter le d�passement par la droite
    if (LabelStun2->Left >340) LabelStun2->Caption = "";
    if (LabelStun3->Left >340) LabelStun3->Caption = "";
    if (LabelStun4->Left >340) LabelStun4->Caption = "";

    // Traitement du cas de double vitesse.
    int dV1, dV2, dV3, dV4;
    dV1 = iV1 /2;        // Limite Mauve bleue
    dV2 = iV2 - dV1;     // Limite Bleu vert
    dV3 = iV3 - dV1;     // Limite Vert Jaune
    dV4 = iV4 - dV1;     // Limite Jaune Mauve

    ImageMJB2->Hide();
    ImageBV2->Hide();
    ImageVJ2->Hide();
    ImageVM2->Hide();
    ImageVB2->Hide();
    ImageJM2->Hide();
    ImageJB2->Hide();
    ImageJV2->Hide();
    ImageSpectrelf->Hide();
    int dX1, dX2, dX3, dX4;
    dX1 = floor( (dV1-500)/2 );
    dX2 = floor( (dV2-500)/2 );
    dX3 = floor( (dV3-500)/2 );
    dX4 = floor( (dV4-500)/2 );

    if (dX1<5) dX1 = 5; // Toujours montrer un peu de la couleur
    if (dX2<5) dX2 = 5;
    if (dX3<5) dX3 = 5;
    if (dX4<5) dX4 = 5;
    PanelSx2->Top  = PanelS->Top+1;
    PanelSx2->Left = PanelS->Left+1;
    PanelSx2->Height = 17;
    PanelSx2->Show();    
    if(dV4 <500)
    {   // Double stun impossible � atteindre
        PanelSx2->Hide();
        //Label7->Caption = "Pas de double speed: " + AnsiString(dV4);
    }else if (dV3 < 500) //dV3: VertJaune
    {   // Seule la barre Jaunex2 est visible
        ImageS1d->Hide();
        ImageS2d->Hide();
        ImageS3d->Hide();
        if (dV4 < iV1)
        {   // La Mauve reste visible
            ImageS4d->Left = 0;
            ImageS4d->Width = dX4 - 3;
            ImageS4d->Show();
            PanelSx2->Width = dX4 + 3;
            //Rajouter image de transition JM
            ImageJM2->Left = PanelSx2->Width - 6;
            ImageJM2->Show();
            // Placer le LabelStun0
            LabelStun0->Show();
            LabelStun0->Caption = dV4;
            LabelStun0->Left = 8 + dX4 - (LabelStun0->Width/2);
            if (LabelStun0->Left < 3)
            {   LabelStun0->Left = 3;
            }
            if ((LabelStun0->Left + LabelStun0->Width + 4) > LabelStun1->Left)
            {   LabelStun0->Left = LabelStun1->Left - (LabelStun0->Width +4);
            }
            if (LabelStun0->Left < 3)
            {   LabelStun0->Hide();
            }else LabelStun0->Show();
            //Label7->Caption=("Tag J1: JMBVJN "+ AnsiString(dX3) +" "+AnsiString(dX4));
        }else if ((iV1 >= 500) && (iV1 <= dV4) && (dV4 < iV2))
        {   // Mauve cach�, le jaunex2 arrive sur le Bleu ou le Vert
            if (iV1 == 500)
            {   // Cas du Spectre, pile � 500, que l'image de transition adapt�e
                ImageSpectrelf->Left = 0;
                ImageSpectrelf->Show();
                PanelSx2->Width = 10;       //ImageSpectrelf
            }else
            {   ImageS4d->Left = 0;
                ImageS4d->Width = dX4 - 3;
                ImageS4d->Show();
                PanelSx2->Width = iX1;
                //Rajouter image de transition JB
                ImageJB2->Left = PanelSx2->Width - 6;
                ImageJB2->Show();
            }
            // du coup r�utiliser le LabelStun1
            LabelStun1->Caption = iV1;
            LabelStun1->Left = 8 + iX1 - (LabelStun1->Width/2);
            if (LabelStun1->Left < 3)
            {   LabelStun1->Left = 3;
            }
            if ((LabelStun1->Left + LabelStun1->Width + 4) > LabelStun2->Left)
            {   LabelStun1->Left = LabelStun2->Left - (LabelStun1->Width +4);
            }
            if (LabelStun1->Left < 3)
            {   LabelStun1->Hide();
            }else LabelStun1->Show();
            //Label7->Caption=("Tag J2: JBVJN "+ AnsiString(ImageS4d->Width) +" "+AnsiString(PanelSx2->Width));

        }else
        {   //Label7->Caption=("Tag J4: NoCode "+ AnsiString(dV4) +" "+AnsiString(iV1));
            PanelSx2->Hide();
        }
    }else if (dV2 < 500) //dV2: BleuVert
    {   // Les barres vertex2 et jaunex2 sont visibles
        ImageS1d->Hide();
        ImageS2d->Hide();
        if (dV3 < iV1)
        {   // La Mauve reste visible, Jaune arrive sur Mauve
            ImageS3d->Left = 0;
            ImageS3d->Width = dX3 - 3;
            ImageS3d->Show();
            //Rajouter image de transition VJ
            ImageVJ2->Left = ImageS3d->Width;
            ImageVJ2->Show();
            ImageS4d->Left = ImageS3d->Width + 6;
            ImageS4d->Width = (dX4 - ImageS4d->Left) - 3;
            ImageS4d->Show();
            PanelSx2->Width = dX4 + 3;
            //Rajouter image de transition JM
            ImageJM2->Left = PanelSx2->Width - 6;
            ImageJM2->Show();
            // Placer le LabelStun0
            LabelStun0->Show();
            LabelStun0->Caption = dV4;
            LabelStun0->Left = 8 + dX4 - (LabelStun0->Width/2);
            if (LabelStun0->Left < 3)
            {   LabelStun0->Left = 3;
            }
            if ((LabelStun0->Left + LabelStun0->Width + 4) > LabelStun1->Left)
            {   LabelStun0->Left = LabelStun1->Left - (LabelStun0->Width +4);
            }
            if (LabelStun0->Left < 3)
            {   LabelStun0->Hide();
            }else LabelStun0->Show();
            //Label7->Caption=("Tag JV1: "+ AnsiString(dX3) +" "+AnsiString(dX4));
        }else if ((iV1 >=500) && (iV1 <= dV3) && (dV3 < iV2))
        {   // Mauve cach�, Bleu visible, Jaune arrive sur Bleu
            ImageS3d->Left = 0;
            ImageS3d->Width = dX3 - 3;
            ImageS3d->Show();
            //Rajouter image de transition VJ
            ImageVJ2->Left = ImageS3d->Width;
            ImageVJ2->Show();
            ImageS4d->Left = ImageS3d->Width + 6;
            ImageS4d->Width = (dX4 - ImageS4d->Left) - 3;
            ImageS4d->Show();
            PanelSx2->Width = iX1;
            //Rajouter image de transition JB
            ImageJB2->Left = PanelSx2->Width - 6;
            ImageJB2->Show();
            // du coup r�utiliser le LabelStun1
            LabelStun1->Caption = iV1;
            LabelStun1->Left = 8 + iX1 - (LabelStun1->Width/2);
            if (LabelStun1->Left < 3)
            {   LabelStun1->Left = 3;
            }
            if ((LabelStun1->Left + LabelStun1->Width + 4) > LabelStun2->Left)
            {   LabelStun1->Left = LabelStun2->Left - (LabelStun1->Width +4);
            }
            if (LabelStun1->Left < 3)
            {   LabelStun1->Hide();
            }else LabelStun1->Show();
            //Label7->Caption=("Tag JV2: "+ AnsiString(iX1) +" "+AnsiString(dX4));
        }else
        {   //Label7->Caption=("Tag JV4: NoCode "+ AnsiString(dV4) +" "+AnsiString(iV1));
            PanelSx2->Hide();
        }
    }else if (dV1 < 500) // dV1: MauveBleu
    {   // La barre bleue x2 est visible
        ImageS1d->Hide();
        if (dV3 < iV1)
        {   // Jaunex2 arrive sur Mauve qui reste visible
            ImageS2d->Left = 0;
            ImageS2d->Width = dX2 - 3;
            ImageS2d->Show();
            // image de transition BV
            ImageBV2->Left = ImageS2d->Width;
            ImageBV2->Show();
            ImageS3d->Left = ImageS2d->Width + 6;
            ImageS3d->Width = (dX3 - ImageS3d->Left) - 3;
            ImageS3d->Show();
            // image de transition VJ
            ImageVJ2->Left = ImageS3d->Left + ImageS3d->Width;
            ImageVJ2->Show();
            ImageS4d->Left = ImageS3d->Left + ImageS3d->Width + 6;
            ImageS4d->Width = (dX4 - ImageS4d->Left) - 3;
            ImageS4d->Show();
            PanelSx2->Width = dX4 + 3;
            // image de transition JM
            ImageJM2->Left = PanelSx2->Width - 6;
            ImageJM2->Show();
            // Placer le LabelStun0
            LabelStun0->Show();
            LabelStun0->Caption = dV4;
            LabelStun0->Left = 8 + dX4 - (LabelStun0->Width/2);
            if (LabelStun0->Left < 3)
            {   LabelStun0->Left = 3;
            }
            if ((LabelStun0->Left + LabelStun0->Width + 4) > LabelStun1->Left)
            {   LabelStun0->Left = LabelStun1->Left - (LabelStun0->Width +4);
            }
            if (LabelStun0->Left < 3)
            {   LabelStun0->Hide();
            }else LabelStun0->Show();
            //Label7->Caption=("Tag BVJN1: "+ AnsiString(ImageBV2->Left) +" "+AnsiString());
        }else if ((iV1 >=500) && (iV1 <= dV3) && (dV3 < iV2))
        {   // Jaunex2 arrive sur Bleu
            ImageS2d->Left = 0;
            ImageS2d->Width = dX2 - 3;
            ImageS2d->Show();
            // image de transition BV
            ImageBV2->Left = ImageS2d->Width;
            ImageBV2->Show();
            ImageS3d->Left = ImageS2d->Width + 6;
            ImageS3d->Width = (dX3 - ImageS3d->Left) - 3;
            ImageS3d->Show();
            // image de transition VJ
            ImageVJ2->Left = ImageS3d->Left + ImageS3d->Width;
            ImageVJ2->Show();
            ImageS4d->Left = ImageS3d->Left + ImageS3d->Width + 6;
            ImageS4d->Width = (dX4 - ImageS4d->Left) - 3;
            ImageS4d->Show();
            PanelSx2->Width = iX1;
            // image de transition JB
            ImageJB2->Left = PanelSx2->Width - 6;
            ImageJB2->Show();
            // du coup r�utiliser le LabelStun1
            LabelStun1->Caption = iV1;
            LabelStun1->Left = 8 + iX1 - (LabelStun1->Width/2);
            if (LabelStun1->Left < 3)
            {   LabelStun1->Left = 3;
            }
            if ((LabelStun1->Left + LabelStun1->Width + 4) > LabelStun2->Left)
            {   LabelStun1->Left = LabelStun2->Left - (LabelStun1->Width +4);
            }
            if (LabelStun1->Left < 3)
            {   LabelStun1->Hide();
            }else LabelStun1->Show();
            // Et placer le LabelStun0
            LabelStun0->Show();
            LabelStun0->Caption = dV3;
            LabelStun0->Left = 8 + dX3 - (LabelStun0->Width/2);
            if (LabelStun0->Left < 3)
            {   LabelStun0->Left = 3;
            }
            if ((LabelStun0->Left + LabelStun0->Width + 4) > LabelStun1->Left)
            {   LabelStun0->Left = LabelStun1->Left - (LabelStun0->Width +4);
            }
            if (LabelStun0->Left < 3)
            {   LabelStun0->Hide(); 
                //Label7->Caption=("Tag BVJN2: "+ AnsiString(dX3) +" "+AnsiString(dX4));
            }
      /*  else if ((iV2 <= dV3) && (dV3 < iV3))
        {   // La Mauve est cach�
            ImageS2d->Left = 0;
            ImageS2d->Width = dX2 - 3;
            ImageS2d->Show();
            // image de transition BV
            ImageBV2->Left = ImageS2d->Width;
            ImageBV2->Show();
            ImageS3d->Left = ImageS2d->Width + 6;
            ImageS3d->Width = (dX3 - ImageS3d->Left) - 3;
            ImageS3d->Show();
            // image de transition VJ
            ImageVJ2->Left = ImageS3d->Left + ImageS3d->Width;
            ImageVJ2->Show();
            ImageS4d->Left = ImageS3d->Left + ImageS3d->Width + 6;
            ImageS4d->Width = (dX4 - ImageS4d->Left) - 3;
            ImageS4d->Show();
            PanelSx2->Width = iX2;
            // image de transition JV
            ImageJV2->Left = PanelSx2->Width - 6;
            ImageJV2->Show();
            //Label7->Caption=("Tag BVJN3: "+ AnsiString(dX3) +" "+AnsiString(dX4));
             */
        } else
        {   //Label7->Caption=("Tag: NoCode "+ AnsiString(dV4) +" "+AnsiString(iV1));
            PanelSx2->Hide();
        }

    }else
    {
   /*AnsiString s = "dV1: " + AnsiString(dV1)
                + ", dV2: " + AnsiString(dV2)
                + ", dV3: " + AnsiString(dV3)
                + ", dV4: " + AnsiString(dV4)
                + "\riV1: " + AnsiString(iV1)
                + ", iV2: " + AnsiString(iV2)
                + ", iV3: " + AnsiString(iV3)
                + ", iV4: " + AnsiString(iV4); 
    Label7->Caption=s;     */
    }
    Fin_Afficher_Marqueurs:


    // Bulle Kitting
    int iK1, iK2;
    if ((iVK1 < 0)||(iVK1> iVK2)||(iVK1<500)||(iVK2>1200))
    {   // masquer la barre kitting
        ShapeSK->Hide();
        //ShapeSK->Left = 252;
        //ShapeSK->Width = 30;
        //ShapeSK->Brush->Color =  clMoneyGreen	;        // Kitting
        //ShapeSK->BringToFront();
    }else
    {   //montrer la barre Kitting
        iK1 = floor( (iVK1-500)/2 );
        iK2 = floor( (iVK2-500)/2 );
        ShapeSK->Show();
        ShapeSK->Left = iK1;
        ShapeSK->Width = iK2 - iK1;
        ShapeSK->Brush->Color = clMoneyGreen	;        // Kitting
        ShapeSK->BringToFront();
    }


    // Marquer de la vitesse du PJ
    int iP1 = floor( (iPos-500)/2 ) -3;
    if (iP1 < 0)          // ramener la bande position sur la barre
    {   iP1 = 0;
    }else if (iP1 > (PanelS->Width-3))
    {   iP1 = PanelS->Width-3;
    }
    ShapePJ->Left = iP1;
    ShapePJ2->Left = iP1;
    ShapePJ->BringToFront();
    ShapePJ2->BringToFront();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{   Close();
}
//---------------------------------------------------------------------------
// Shows range Damage
//---------------------------------------------------------------------------
void __fastcall TForm1::DoRangeDmg(TObject *Sender)
{   LabelS6->Caption = " @"+ AnsiString(Timer2->Tag)
                         +": " + AnsiString(sRangeDmg[Timer2->Tag].iAtk1)
                         +".." + AnsiString(sRangeDmg[Timer2->Tag].iAtk2)
                         +" (" + AnsiString(sRangeDmg[Timer2->Tag].iAtkC)+")";
     Timer2->Tag ++;
    // Enfin retour au d�gats port�e 0, au prochain affichage
    if (Timer2->Tag > iMaxRange) Timer2->Tag = 0;
}
//---------------------------------------------------------------------------
// Changement de Focus
//---------------------------------------------------------------------------
void __fastcall TForm1::CB2Select(TObject *Sender)
{   if (CB2->ItemIndex == 0)    UpDownCB2->Enabled = false;
    else                        UpDownCB2->Enabled = true;
    // R�gler le d�gat des poings pour TMW_BRAWLING
    //ShowMessage("CB2Select");
    if (RadioButton1->Checked == true)       // Preset "Nothing"
    {   if (CB2->ItemIndex == 5) iDmg = CB2->Tag/3;
        else                     iDmg = 0;
    }else                                    // pas le Preset "Nothing"
    {   if (CB2->ItemIndex == 5)
        {   CB2->ItemIndex = 0;
            MessageDlg("Brawling focus skill can only\rbe set with Nothing preset!", mtError, TMsgDlgButtons() << mbOK, 0);
            return;
    }   }
    this->Anything_Changed(this);
    switch (CB2->ItemIndex)
    {   case 1: //iSkillStat = iInt;
                LabeledEditO3->Color = clHighlight; // Astral soul
                Timer1->Enabled = true;
                CB2->Hint ="Needs intelligence. Playing mage without Astral Soul is useless above 50 int & level!";
            break;
        case 2: //iSkillStat = iDex;
                //LabeledEditD3->Color = clHighlight; // Malard's eye
                Timer1->Enabled = true;
                CB2->Hint ="Needs dexterity. Improve range, hit chances, and long range damage.";
            break;
        case 3: //iSkillStat = iAgi;
                LabeledEditD3->Color = clHighlight; // Speed
                LabeledEditO5->Color = clHighlight;
                Timer1->Enabled = true;
                CB2->Hint ="Needs agility. Improve evasion, dodge, attack and walking speed.";
            break;
        case 4: //iSkillStat = iStr;
                LabeledEditO4->Color = clHighlight; // Raging
                LabeledEditD7->Color = clHighlight;
                Timer1->Enabled = true;
                CB2->Hint ="Needs strength. Improve critical rate, but reduce damage absobsion.";
            break;
        case 5: //iSkillStat = iAgi;
                LabeledEditO1->Color = clHighlight; // Brawling
                LabeledEditD3->Color = clHighlight;
                Timer1->Enabled = true;
                CB2->Hint ="Needs agility. When unarmed, improve hit chances, damage, evasion and dodge.";
            break;
        default:iSkillStat = 0;
                CB2->Hint ="Select your Focus Skill.";
            break;
    }
    // Corriger la position du UpDown
    int i;
    AnsiString s = CB2->Text;
    s = s.SubString(s.Length()-1, 2);
    i = s.ToIntDef(9);
    UpDownCB2->Position = i;     
    // Calculer le SkillValue
    vCalcSkillValue(this);
    CheckMaxRange(this, Button1->Tag);
}
//---------------------------------------------------------------------------
// Changement du niveau du Focus
//---------------------------------------------------------------------------
void __fastcall TForm1::UpDownCB2Click(TObject *Sender, TUDBtnType Button)
{   if (CB2->ItemIndex <=0) return;
    // Corriger le texte du CB2
    int i = CB2->ItemIndex;
    AnsiString s = CB2->Text;
    s.SetLength(s.Length()-2);
    s = s+ " " + AnsiString(UpDownCB2->Position);
    CB2->Items->Strings[CB2->ItemIndex] = s;
    CB2->ItemIndex = i;
    // Calculer le SkillValue
    vCalcSkillValue(this);
    CheckMaxRange(this, Button1->Tag);
    CB2Select(this);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------   
//---------------------------------------------------------------------------
// Calculs de mezz ou stun
//---------------------------------------------------------------------------
void __fastcall TForm1::EstimerStun(TObject *Sender)
{   // on y est !
    int iThinkTime = 100;
    int iSpeed, iaDelay, iaMotion, idMotion;
    int p = CB1->ItemIndex;
    if (p == -1) p = 0;
    iSpeed =  sMonsters[p].iSpeed;
    iaMotion = sMonsters[p].iAdelay;
    idMotion =  sMonsters[p].idMotion;
    if ((RadioButton3->Checked)||(RadioButton5->Checked))  // Show Spell damage
    {   iaDelay = (((200 - iAgi) * 1200) / 200);   //Wand spell delay
    }else
    {   iaDelay = iHitDelay *2;
    }
    int iV1  = idMotion;
    int iV2  = idMotion + iSpeed/4;
    int iV3  = idMotion + (iSpeed*1.4)/4;
    int iV4  = idMotion + (iSpeed*1.4)/4 +100;
    int iVK1 = -1; //TBD
    int iVK2 = -1; //TBD
    int iPos = iaDelay;
    if ((sMonsters[p].iMode==0)||(sMonsters[p].iMode==128)) iV1 = -99; // Static mob
    if (RadioButton4->Checked)                                      // Archer
    {   if (sMonsters[p].iRange >= iMaxRange) iV1 = -98;
    }else if ((RadioButton3->Checked)||(RadioButton5->Checked))     // Mage
    {   if (sMonsters[p].iRange > 2) iV1 = -98; // #confringo:3
    }else if (RadioButton3->Checked)                                // Sans armes
    {   if (sMonsters[p].iRange > 1) iV1 = -98;  // Mains nues:2
    }else                                                           // Arme C�C
    {   if (sMonsters[p].iRange > 1) iV1 = -98; // Arme simple:2
    }
    PanelStun(this, iV1, iV2, iV3, iV4, iVK1, iVK2, iPos);

    /*
    // Statistiques:
    int iStart = 33;            // Instant ou le mob est bless� la 1ere fois

    bool bCanMoveState = true;  // Bascule pour la boucle
    int iMove = -1;             // Instant ou le mob aura le droit de bouger
    int iWalk = -1;             // Instant ou le mob bougera effectivement

    int itStun = iStart;        // Nbe de ms ou le mob ne peut pas bouger apr�s iStart
    int itMouv = 0;             // Nbe de ms ou le mob peut bouger apr�s iStart
                                // mais il ne bouge pas forcement car il faut attendre speed/4

    int itAtk = 0;              // Nbe d'attaques port�es au mob
    int itWalk = 0;             // Nbe de d�placements du mob
    int itAtk1 = 0;             // Nbe de coups cons�cutifs avant que le mob ne bouge
    int itAtk1max = 0;          // Nbe maxi de coups cons�cutifs avant que le mob ne bouge



    int itTotal = iStart;
    for (int i=iStart; i< 600000; i++)     // en ms = 10 minutes
    {   // Rechercher le Timer principal
        if ((i % iThinkTime) == 0)
        {   // Placer si besoin, le Timer Walk
            if (bCanMoveState)
            {   if (iWalk == -1) iWalk = i + iSpeed/4;
            }
        }
        // Rechercher l'instant de la blessure
        if ((i % iaDelay) == iStart)
        {   if (i > iMove)
            {   iMove = i + idMotion;
                bCanMoveState = false;
            }
            // De toute fa�on la blessure interrompt le Timer Walk en cours
            iWalk = -1;
            itAtk ++; // Compteur d'attaques
            itAtk1 ++;
             //ShowMessage("Blessure: "+AnsiString(itAtk)+":  "+AnsiString(i));
        }
        // Rechercher la fin de l'immobilisation
        if (i == iMove)
        {   bCanMoveState = true;
            iMove =-1;
        }
        // Rechercher le d�placement
        if (i == iWalk)
        {   iWalk = i + iSpeed/2;
            itWalk++;  // Compteur de mouvements
            //ShowMessage("Coups avant que ca bouge: "+AnsiString(itAtk1)+":  "+AnsiString(i));
            if (itAtk1 > itAtk1max) itAtk1max = itAtk1;
            itAtk1 = 0;
        }
        // Stats  en ms stun ou d�placements possibles
        if (iWalk < i) itStun++;
        else           itMouv++;
        itTotal = itTotal + 1;
    }


    AnsiString s = "\rTemps de stun: " + AnsiString(itStun/1000.0)
                 + "s, temps mobile: " + AnsiString(itMouv/1000.0)
                 + "s, itTotal: " + AnsiString(itTotal/1000.0) +"s / "+ AnsiString((itStun+itMouv)/1000.0)
                 + "s\rNbe coups: " + AnsiString(itAtk)
                 + ", Nbe maxi cons�cutifs: " + AnsiString(itAtk1max)
                 + ", nbe pas: " + AnsiString(itWalk);
    //ShowMessage (s);
    */



}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormShow(TObject *Sender)
{   // Redessiner la barre des vitesse car sans ca elle bugue al�atoirement
   // EstimerStun(this);
}
//---------------------------------------------------------------------------

