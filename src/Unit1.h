//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "trayicon.h"
#include <Buttons.hpp>
#include "math.h"
#include <Dialogs.hpp>
#include <Menus.hpp>
#include <Graphics.hpp>

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TLabeledEdit *LabeledEdit1;
        TUpDown *UpDown1;
        TLabeledEdit *LabeledEdit2;
        TLabeledEdit *LabeledEdit3;
        TLabeledEdit *LabeledEdit4;
        TLabeledEdit *LabeledEdit5;
        TLabeledEdit *LabeledEdit6;
        TUpDown *UpDown2;
        TUpDown *UpDown3;
        TUpDown *UpDown4;
        TUpDown *UpDown5;
        TUpDown *UpDown6;
        TEdit *Edit1;
        TLabel *LabelB;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *Edit2;
        TLabel *Label5;
        TLabel *Label6;
        TLabeledEdit *LabeledEditL;
        TUpDown *UpDownL;
        TLabeledEdit *LabeledEditP;
        TBitBtn *BitBtn1;
        TUpDown *UpDown11;
        TUpDown *UpDown12;
        TUpDown *UpDown13;
        TEdit *Edit3;
        TEdit *Edit4;
        TEdit *Edit5;
        TEdit *Edit6;
        TLabel *Label4;
        TLabel *Label3;
        TUpDown *UpDown14;
        TUpDown *UpDown15;
        TUpDown *UpDown16;
        TLabeledEdit *LabeledEditHP;
        TLabeledEdit *LabeledEditMP;
        TGroupBox *GroupBox2;
        TLabeledEdit *LabeledEditD3;
        TGroupBox *GroupBox3;
        TLabeledEdit *LabeledEditO1;
        TLabeledEdit *LabeledEditO2;
        TLabeledEdit *LabeledEditO3;
        TLabeledEdit *LabeledEditD5;
        TLabeledEdit *LabeledEditD4;
        TLabeledEdit *LabeledEditD1;
        TLabeledEdit *LabeledEditO4;
        TLabeledEdit *LabeledEditO5;
        TLabeledEdit *LabeledEditD6;
        TLabeledEdit *LabeledEditW;
        TLabeledEdit *LabeledEditO6;
        TGroupBox *GroupBox4;
        TRadioButton *RadioButton1;
        TRadioButton *RadioButton2;
        TRadioButton *RadioButton3;
        TRadioButton *RadioButton4;
        TRadioButton *RadioButton5;
        TRadioButton *RadioButton6;
        TRadioButton *RadioButton7;
        TLabel *LabelFA;
        TStatusBar *StatusBar1;
        TLabeledEdit *LabeledEditD2;
        TLabeledEdit *LabeledEditD7;
        TLabel *LabelDmg;
    TGroupBox *GroupBox5;
    TComboBox *CB1;
    TLabel *LabelS1;
    TLabel *LabelS3;
    TLabel *LabelS2;
    TLabel *LabelS4;
    TLabel *LabelS5;
    TLabel *LabelS7;
    TLabel *LabelS8;
    TLabel *LabelS9;
    TLabel *LabelS10;
    TLabel *LabelS11;
    TTimer *Timer1;
    TLabel *Label7;
    TOpenDialog *OpenDialog1;
    TPopupMenu *PopupMenu1;
    TMenuItem *Lod1;
    TMenuItem *Default1;
    TPopupMenu *PopupMenuCaC;
    TMenuItem *Knife1;
    TMenuItem *Sharpknife1;
    TMenuItem *Dagger1;
    TMenuItem *ShortSword1;
    TMenuItem *Icegladius1;
    TMenuItem *BoneKnife1;
    TMenuItem *N1;
    TMenuItem *Scyte1;
    TMenuItem *Setzer1;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *Label10;
    TButton *Button1;
    TPopupMenu *PopupMenuRange;
    TMenuItem *MenuItem12;
    TMenuItem *MenuItem13;
    TMenuItem *MenuItem14;
    TMenuItem *MenuItem15;
    TMenuItem *MenuItem16;
    TLabel *LabelIG;
    TMenuItem *RockKnife1;
    TLabel *LabelRK;
    TPanel *Panel4;
    TPanel *Panel1;
    TPanel *Panel2;
    TPanel *Panel3;
    TLabel *LabelS13;
    TButton *Button2;
    TPanel *Panel8;
    TPanel *Panel7;
    TPanel *Panel6;
    TPanel *Panel5;
    TLabel *LabelS39;
    TLabel *LabelS38;
    TLabel *Label13;
    TLabel *Label11;
    TLabel *LabelS58;
    TLabel *LabelS59;
    TLabel *LabelS57;
    TLabel *LabelS37;
    TPanel *Panel34;
    TPanel *Panel33;
    TPanel *Panel32;
    TPanel *Panel31;
    TPanel *Panel54;
    TPanel *Panel53;
    TPanel *Panel52;
    TPanel *Panel51;
    TButton *Button3;
    TComboBox *CB2;
    TLabel *LabelS3a;
    TLabel *LabelS6;
    TLabeledEdit *LabeledEditDPS;
    TTimer *Timer2;
    TEdit *EditMB;
    TUpDown *UpDownMB;
    TEdit *EditAR;
    TUpDown *UpDownAR;
    TUpDown *UpDownCB2;
    TMemo *Memo1;
    TButton *Button4;
    TPanel *PanelS;
    TImage *ImageMJB;
    TImage *ImageBV;
    TImage *ImageVJ;
    TImage *ImageJN;
    TPanel *Panel9;
    TShape *ShapePJ;
    TImage *ImageS1;
    TImage *ImageS2;
    TImage *ImageS3;
    TImage *ImageS4;
    TImage *ImageS5;
    TShape *ShapeSK;
    TLabel *LabelStun1;
    TLabel *LabelStun2;
    TLabel *LabelStun3;
    TLabel *LabelStun4;
    TPanel *PanelSx2;
    TImage *ImageS1d;
    TImage *ImageS2d;
    TImage *ImageS3d;
    TImage *ImageS4d;
    TImage *ImageVJ2;
    TImage *ImageJM2;
    TImage *ImageVM2;
    TImage *ImageJB2;
    TImage *ImageBV2;
    TImage *ImageMJB2;
    TImage *ImageVB2;
    TImage *ImageJV2;
    TShape *ShapePJ2;
    TLabel *LabelStun0;
    TImage *ImageSpectrelf;
    TImage *ImageWisp;
	void __fastcall DisplayHint(TObject *Sender);
        void __fastcall Stat_OnChange(TObject *Sender);
        void __fastcall Bonus_OnChange(TObject *Sender);
        void __fastcall Level_OnChange(TObject *Sender);
        void __fastcall Anything_Changed(TObject *Sender);
        void __fastcall Reset_Stats(TObject *Sender);
        void __fastcall Player_Select(TObject *Sender);
        void __fastcall CB1Change(TObject *Sender);
    void __fastcall CB1Select(TObject *Sender);
    void __fastcall TrackBar1Change(TObject *Sender);
    void __fastcall Timer1Timer(TObject *Sender);
    void __fastcall LoadMob(TObject *Sender);
    void __fastcall LoadDefautMobs(TObject *Sender);
    void __fastcall Lod1Click(TObject *Sender);
    void __fastcall Default1Click(TObject *Sender);
    void __fastcall PopupArme(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall PanelMob(TObject *Sender, int iV1, int iV2, int iV3);
    void __fastcall PanelMob3(TObject *Sender, int iV1, int iV2, int iV3);
    void __fastcall PanelMob5(TObject *Sender, int iV1, int iV2, int iV3);
    void __fastcall PanelPJ(TObject *Sender, int iV1, int iV2, int iV3);      
    void __fastcall PanelStun(TObject *Sender, int iV1, int iV2, int iV3, int iV4, int iVK1, int iVK2, int iPos);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall DoRangeDmg(TObject *Sender);
    void __fastcall EditMBChange(TObject *Sender);
    void __fastcall CB2Select(TObject *Sender);
    void __fastcall UpDownCB2Click(TObject *Sender, TUDBtnType Button);
    void __fastcall Memo1Click(TObject *Sender);
    void __fastcall Button4Click(TObject *Sender);
    void __fastcall EstimerStun(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
    void __fastcall DoShowHint(System::AnsiString &HintStr, bool &CanShow, THintInfo &HintInfo);
    void __fastcall TForm1::vCalcSkillValue(TComponent* Owner);
    void __fastcall TForm1::CheckMaxRange(TComponent* Owner, int );
        int iStr;
        int iAgi;
        int iVit;
        int iInt;
        int iDex;
        int iLuc;
        int iLev;       // Level
        int iSPt;       // Status Points
        int iStrB;
        int iAgiB;
        int iVitB;
        int iIntB;
        int iDexB;
        int iLucB;
        int iArmor;
        int iDmg;
        int iDmgView;
        int iMAtk1;     // Valeur totale, Stats + Equipement  Semble inutile pour TMW
        int iMAtk2;     // Valeur totale, Stats + Equipement
        int iMagic;     // Le bonus, du � l'�quipement
        int iCritCh;    // attention, sur 1000.
        float fRawDPS;
        int iArrowDmg;
        int iSkillStat;
struct tMonsters {
  AnsiString sName;
  int iLV, iHP, iRange, iATK1, iATK2, iSTR, iDEF, iMDef, iAGI, iVIT, iINT, iDEX, iLUK, iElem, iMode, iSpeed, iAdelay, idMotion;
} sMonsters[50];
        __fastcall TForm1(TComponent* Owner);
        int iHitDelay;
        struct tRangeDmg {int iAtk1, iAtk2, iAtkC;
        } sRangeDmg[10];
        int iMaxRange;     // Only for Bow or Sling attacks

};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
 