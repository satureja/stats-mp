# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.6.1] - 2017-12-19
### Changed
- More helpful mob hints.

### Fixed
- Elemental resistances versus spells.

## [1.6.0] - 2017-12-12
### Added
- Stun meter

## [1.5.0] - 2017-11-28
### Added
- License.
- Changelog.
- Readme.

## 1.4.1 - 2017-11-23
### Added
- Stun effect for spells.

### Fixed
- `#confringo` with staff.

## 1.4.0 - 2017-11-08
### Added
- Focus skills.

### Changed
- Misc improvements.


[Unreleased]: https://gitlab.com/satureja/stats-mp/compare/v1.6.0...HEAD
[1.6.1]: https://gitlab.com/satureja/stats-mp/compare/v1.6.0...v1.6.1
[1.6.0]: https://gitlab.com/satureja/stats-mp/compare/v1.5.0...v1.6.0
[1.5.0]: https://gitlab.com/satureja/stats-mp/compare/v1.5.0~1...v1.5.0
