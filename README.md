# Stats and DPS calculator.
Stats calculator for The Mana World Legacy.

## Notes
- DPS is only computed for weapon attack.
- When using Bow/Sling you're suposed to be at max range minus one.
- Mages DPS is shown for their weapon attack, but spell damage is shown on current mob.
- `#confringo` is half damage on mob without any resistance....

## Instructions
1. On "Choose your server" window, select `The Mana World`, then click <kbd>Info</kbd>.
2. Download Server data
3. In world/map/db, you'll find mob definition files
4. Load them as needed with StatMP
